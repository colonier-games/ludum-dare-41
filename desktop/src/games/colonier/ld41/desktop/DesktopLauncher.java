package games.colonier.ld41.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import games.colonier.ld41.LD41Game;
import games.colonier.ld41.config.GraphicsConfig;
import games.colonier.ld41.config.InputConfig;
import games.colonier.ld41.config.PersistedConfig;

public class DesktopLauncher {
	public static void main (String[] arg) {

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		PersistedConfig.load ();

		config.width = GraphicsConfig.getInstance ().getResolutionWidth ();
		config.height = GraphicsConfig.getInstance ().getResolutionHeight ();

		config.samples = GraphicsConfig.getInstance ().getAaLevel ();

		config.foregroundFPS = 0;
		config.backgroundFPS = 0;
		config.vSyncEnabled = GraphicsConfig.getInstance ().isVsyncEnabled ();

		config.fullscreen = GraphicsConfig.getInstance ().isFullScreen ();

		new LwjglApplication(new LD41Game(), config);

	}
}
