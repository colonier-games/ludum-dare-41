package games.colonier.ld41;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.GdxAI;
import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.graphics.GL20;
import games.colonier.ld41.config.InputConfig;
import games.colonier.ld41.constants.*;
import games.colonier.ld41.state.GameApp;
import games.colonier.ld41.state.GameState;
import games.colonier.ld41.state.impl.MainMenuStateImpl;
import games.colonier.ld41.state.impl.PlayStateImpl;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.reflect.Constructor;

public class LD41Game extends ApplicationAdapter implements GameApp {

    private Reflections reflections;

    private GameState nextState;
    private GameState currentState;

    private float appTimer;
    private float lastFrameTime;
    private float lastRenderTime;
    private float lastUpdateTime;

    private int frameCount;
    private int fps;
    private float fpsTimer;

    private void initReflections () {

        ConfigurationBuilder cfgBuilder = new ConfigurationBuilder ();

        cfgBuilder.setUrls (
                ClasspathHelper.forPackage ("games.colonier.ld41")
        );
        cfgBuilder.addClassLoader (getClass ().getClassLoader ());
        cfgBuilder.setScanners (
                new SubTypesScanner (false),
                new TypeAnnotationsScanner ()
        );

        reflections = new Reflections (cfgBuilder);

    }

    @Override
    public void create () {

        InputConfig.getInstance ()
                .init ();

        TileTextures.initialize ();
        ObjectTextures.initialize ();
        Fonts.initialize ();
        Musics.initialize ();
        Sounds.initialize ();

        initReflections ();

        currentState = new MainMenuStateImpl (
                reflections,
                this
        );

        currentState.init ();

    }

    private void tick () {

        GdxAI.getTimepiece ().update (Gdx.graphics.getDeltaTime ());
        MessageManager.getInstance ().update ();

        appTimer += Gdx.graphics.getDeltaTime ();

        currentState.tick ();

    }

    private void handleFps () {

        ++fps;

        fpsTimer += Gdx.graphics.getDeltaTime ();

        if (fpsTimer >= 1.0f) {

            System.out.println ("FPS: " + fps);

            fpsTimer = 0f;
            fps = 0;

        }

    }

    @Override
    public void render () {

        if (nextState != null) {

            currentState.dispose ();

            nextState.init ();

            currentState = nextState;
            nextState = null;

        }

        long timeBeforeUpdate = System.currentTimeMillis ();

        tick ();

        long timerAfterUpdate = System.currentTimeMillis ();

        lastUpdateTime = (float) ( timerAfterUpdate - timeBeforeUpdate ) / 1000f;

        if (nextState == null) {

            long timeBeforeRender = System.currentTimeMillis ();

            Gdx.gl.glClearColor (1f, 1f, 1f, 1);
            Gdx.gl.glClear (GL20.GL_COLOR_BUFFER_BIT);

            currentState.render ();

            long timeAfterRender = System.currentTimeMillis ();

            lastRenderTime = (float) ( timeAfterRender - timeBeforeRender ) / 1000f;
            lastFrameTime = (float) ( timeAfterRender - timeBeforeUpdate ) / 1000f;

        }

        handleFps ();

        ++frameCount;

    }

    @Override
    public void dispose () {


    }

    @Override
    public GameState getCurrentState () {
        return currentState;
    }

    @Override
    public void changeState (Class <? extends GameState> newStateClass) {
        try {

            Constructor <? extends GameState> constructor
                    = newStateClass.getConstructor (
                    Reflections.class,
                    GameApp.class
            );

            Object newGameState = constructor.newInstance (
                    reflections,
                    this
            );

            nextState = (GameState) newGameState;

        } catch (Exception exc) {

            throw new RuntimeException ("Couldn't initialize new game state, reason: ", exc);

        }

    }

    @Override
    public float getAppTimer () {
        return appTimer;
    }

    @Override
    public float getLastFrameTime () {
        return lastFrameTime;
    }

    @Override
    public float getLastRenderTime () {
        return lastRenderTime;
    }

    @Override
    public float getLastUpdateTime () {
        return lastUpdateTime;
    }

    @Override
    public int getFrameCount () {
        return frameCount;
    }

    @Override
    public int getFps () {
        return fps;
    }
}
