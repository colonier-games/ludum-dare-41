package games.colonier.ld41.entity;

import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.config.DifficultyConfig;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.laser.Reflector;
import games.colonier.ld41.state.spec.PlayState;

import java.io.Serializable;

public class Player extends AbstractGameEntity implements Serializable {

    public float rotation = 0.0f;
    public Movement movement = new Movement ();
    public Reflector playerReflector;

    public int deaths = 0;
    public int kills = 0;

    public Player (int id, PlayState playState, Vector2 position) {
        super (id, DifficultyConfig.getInstance ().getDifficulty ().playerHealth, playState);
        this.position.set (position);

        this.playerReflector = new Reflector (
                position.cpy (),
                new Vector2 (0, 1),
                Numbers.TILE_SCALE / 2f
        );

        this.absorber
                .setRadius (
                        Numbers.TILE_SCALE / 2f - 2f
                );

    }

    public final static class Movement {

        public boolean moveUp;
        public boolean moveDown;
        public boolean moveRight;
        public boolean moveLeft;

    }

}
