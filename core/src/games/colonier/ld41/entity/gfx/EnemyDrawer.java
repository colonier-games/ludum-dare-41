package games.colonier.ld41.entity.gfx;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.constants.ObjectTextures;
import games.colonier.ld41.entity.Enemy;
import games.colonier.ld41.entity.enemy.circler.CirclerEnemy;
import games.colonier.ld41.entity.enemy.circler.CirclerEnemyDrawer;
import games.colonier.ld41.entity.enemy.simple.SimpleEnemy;
import games.colonier.ld41.entity.enemy.simple.SimpleEnemyDrawer;

import java.util.HashMap;
import java.util.Map;

public class EnemyDrawer extends EntityDrawer <Enemy>  {

    private Map <Class <? extends Enemy>, EntityDrawer> specificEnemyDrawers = new HashMap <> ();

    public EnemyDrawer () {

        registerSpecificEnemyDrawers ();

    }

    private void registerSpecificEnemyDrawers () {

        specificEnemyDrawers.put (
                SimpleEnemy.class,
                new SimpleEnemyDrawer ()
        );

        specificEnemyDrawers.put (
                CirclerEnemy.class,
                new CirclerEnemyDrawer ()
        );

    }

    @Override
    public void draw (Enemy entity, SpriteBatch spriteBatch) {

        if (specificEnemyDrawers.containsKey (entity.getClass ())) {

            specificEnemyDrawers.get (entity.getClass ())
                    .draw (
                            entity,
                            spriteBatch
                    );

        } else {

            spriteBatch.draw (
                    ObjectTextures.ENEMY_REGION (),
                    entity.getPosition ().x - Numbers.TILE_SCALE / 2f,
                    entity.getPosition ().y - Numbers.TILE_SCALE / 2f,
                    Numbers.TILE_SCALE / 2f,
                    Numbers.TILE_SCALE / 2f,
                    Numbers.TILE_SCALE,
                    Numbers.TILE_SCALE,
                    1f, 1f,
                    entity.rotation - 90f
            );

        }

    }
}
