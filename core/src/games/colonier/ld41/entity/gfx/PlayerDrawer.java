package games.colonier.ld41.entity.gfx;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.constants.ObjectTextures;
import games.colonier.ld41.entity.Player;

public class PlayerDrawer extends EntityDrawer <Player> {

    @Override
    public void draw (Player player, SpriteBatch spriteBatch) {

        spriteBatch.draw (
                ObjectTextures.PLAYER_REGION (),
                player.position.x - Numbers.TILE_SCALE / 2f,
                player.position.y - Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE,
                Numbers.TILE_SCALE,
                1f, 1f,
                player.playerReflector.getNormal ()
                        .angle () - 90f
        );

    }

}
