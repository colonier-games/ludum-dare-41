package games.colonier.ld41.entity.gfx;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import games.colonier.ld41.entity.GameEntity;

public abstract class EntityDrawer <E extends GameEntity> {

    public abstract void draw (
            E entity,
            SpriteBatch spriteBatch
    );

}
