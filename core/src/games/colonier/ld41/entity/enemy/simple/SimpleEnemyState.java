package games.colonier.ld41.entity.enemy.simple;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.Heuristic;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.config.DifficultyConfig;
import games.colonier.ld41.constants.Messages;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.map.GameMapGraphNode;
import games.colonier.ld41.map.TileCoords;

public enum SimpleEnemyState implements State <SimpleEnemy> {

    LIVING () {

        Vector2 tmp = new Vector2 ();
        Vector2 tmp2 = new Vector2 ();

        @Override
        public void enter (SimpleEnemy entity) {

            if (entity.currentPath == null) {

                calculatePathToPlayer (entity);

            }

        }

        @Override
        public void update (SimpleEnemy entity) {

            if (entity.currentPathIndex >= entity.currentPath.getCount ()) {
                entity.currentPathIndex = entity.currentPath.getCount () - 1;
            }

            GameMapGraphNode node = entity.currentPath
                    .get (
                            entity.currentPathIndex
                    );

            GameMapGraphNode next = node;

            if (entity.currentPathIndex < entity.currentPath.getCount () - 1) {

                next = entity.currentPath.get (entity.currentPathIndex + 1);

            }

            entity.velocity
                    .set (
                            entity.position.x,
                            entity.position.y
                    )
                    .sub (
                            next.coords.x * Numbers.TILE_SCALE + Numbers.TILE_SCALE / 2f,
                            next.coords.y * Numbers.TILE_SCALE + Numbers.TILE_SCALE / 2f
                    ).nor ()
                    .scl (-1f)
                    .scl (Gdx.graphics.getDeltaTime () * DifficultyConfig.getInstance ().getDifficulty ().enemySpeed * Numbers.TILE_SCALE);

            if (node.coords.equals (next.coords)) entity.velocity.set (0, 0);

            if (entity.position.dst (entity.playState.getGamePlayers ().get (0).position) >= Numbers.TILE_SCALE * 3f) {
                entity.position
                        .add (entity.velocity);
            }

            if (
                    entity.position.dst (
                            tmp.set (
                                    next.coords.x * Numbers.TILE_SCALE + Numbers.TILE_SCALE / 2f,
                                    next.coords.y * Numbers.TILE_SCALE + Numbers.TILE_SCALE / 2f
                            )
                    ) <= 2f
                    ) {

                entity.currentPathIndex++;
                if (entity.currentPathIndex >= entity.currentPath.getCount ()) {
                    entity.currentPathIndex = entity.currentPath.getCount () - 1;
                }

            }

            entity.rotation = tmp.set (entity.position)
                    .sub (entity.playState.getGamePlayers ().get (0).position)
                    .nor ().angle () + 180f;

            entity.laser
                    .setDirection (tmp2.set (tmp).scl (-1f));

            entity.laser
                    .setPosition (
                            tmp2.set (tmp)
                                    .scl (Numbers.TILE_SCALE / 2f)
                                    .scl (-1f)
                                    .add (entity.position)
                    );

        }

        @Override
        public void exit (SimpleEnemy entity) {

        }

        @Override
        public boolean onMessage (SimpleEnemy entity, Telegram telegram) {

            if (telegram.message == Messages.PLAYER_MOVED) {
                calculatePathToPlayer (entity);
            }

            return false;
        }
    };

    void calculatePathToPlayer (SimpleEnemy entity) {

        TileCoords tc = new TileCoords (
                (int) ( ( entity.position.x ) / Numbers.TILE_SCALE ),
                (int) ( ( entity.position.y ) / Numbers.TILE_SCALE )
        );

        TileCoords tc2 = new TileCoords (
                (int) ( ( entity.playState.getGamePlayers ()
                        .get (0).position.x ) / Numbers.TILE_SCALE ),
                (int) ( ( entity.playState.getGamePlayers ()
                        .get (0).position.y ) / Numbers.TILE_SCALE )
        );

        entity.currentPath = new DefaultGraphPath <> ();
        entity.currentPathIndex = 0;

        entity.playState
                .getPathFinder ()
                .searchNodePath (
                        entity.playState.getWorld ().gameMap.gameMapGraph.getAtCoords (tc),
                        entity.playState.getWorld ().gameMap.gameMapGraph.getAtCoords (tc2),
                        new Heuristic <GameMapGraphNode> () {
                            @Override
                            public float estimate (GameMapGraphNode node, GameMapGraphNode endNode) {
                                return 1f;
                            }
                        },
                        entity.currentPath
                );

    }

}
