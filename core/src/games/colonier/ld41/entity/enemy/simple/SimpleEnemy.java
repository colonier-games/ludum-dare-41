package games.colonier.ld41.entity.enemy.simple;

import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.entity.Enemy;
import games.colonier.ld41.entity.enemy.EnemyType;
import games.colonier.ld41.state.spec.PlayState;

public class SimpleEnemy extends Enemy <SimpleEnemyState> {

    @Override
    public StateMachine<SimpleEnemy, SimpleEnemyState> createStateMachine () {
        StateMachine <SimpleEnemy, SimpleEnemyState> stateMachine = new DefaultStateMachine <> (
                this,
                SimpleEnemyState.LIVING
        );

        stateMachine.changeState (SimpleEnemyState.LIVING);

        return stateMachine;
    }

    public SimpleEnemy (
            int id, PlayState playState, Vector2 position
    ) {
        super (id, playState, EnemyType.SIMPLE, position);
    }
}
