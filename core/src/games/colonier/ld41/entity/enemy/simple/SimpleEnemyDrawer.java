package games.colonier.ld41.entity.enemy.simple;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.constants.ObjectTextures;
import games.colonier.ld41.entity.gfx.EnemyDrawer;
import games.colonier.ld41.entity.gfx.EntityDrawer;
import games.colonier.ld41.util.TextureUtils;

public class SimpleEnemyDrawer extends EntityDrawer<SimpleEnemy> {

    private Animation <TextureRegion> tankAnimation;
    private float animationTimer = 0f;

    public SimpleEnemyDrawer () {

        tankAnimation = new Animation <TextureRegion> (
                0.125f,
                new Array <> (
                        TextureUtils.loadSingleRowAnimationFrames (
                                "textures/tank-sprite.png",
                                16, 16,
                                0
                        )
                )
        );

        tankAnimation.setPlayMode (Animation.PlayMode.LOOP);

    }

    @Override
    public void draw (SimpleEnemy entity, SpriteBatch spriteBatch) {

        animationTimer += Gdx.graphics.getDeltaTime ();

        spriteBatch.draw (
                tankAnimation.getKeyFrame (animationTimer),
                entity.getPosition ().x - Numbers.TILE_SCALE / 2f,
                entity.getPosition ().y - Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE,
                Numbers.TILE_SCALE,
                1f, 1f,
                entity.rotation - 90f
        );

    }
}
