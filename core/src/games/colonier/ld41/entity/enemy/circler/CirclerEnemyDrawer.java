package games.colonier.ld41.entity.enemy.circler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.entity.gfx.EntityDrawer;
import games.colonier.ld41.util.TextureUtils;

public class CirclerEnemyDrawer extends EntityDrawer <CirclerEnemy> {

    private Animation <TextureRegion> spiderAnimation;
    private float animationTimer = 0f;

    public CirclerEnemyDrawer () {

        spiderAnimation = new Animation <TextureRegion> (
                0.5f,
                new Array <> (
                        TextureUtils.loadSingleRowAnimationFrames (
                                "textures/spider-enemy.png",
                                16, 16,
                                0
                        )
                )
        );

        spiderAnimation.setPlayMode (Animation.PlayMode.LOOP);

    }

    @Override
    public void draw (CirclerEnemy entity, SpriteBatch spriteBatch) {

        animationTimer += Gdx.graphics.getDeltaTime ();

        spriteBatch.draw (
                spiderAnimation.getKeyFrame (animationTimer),
                entity.getPosition ().x - Numbers.TILE_SCALE / 2f,
                entity.getPosition ().y - Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE,
                Numbers.TILE_SCALE,
                1f, 1f,
                entity.rotation - 90f
        );

    }
}
