package games.colonier.ld41.entity.enemy.circler;

import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.entity.Enemy;
import games.colonier.ld41.entity.enemy.EnemyType;
import games.colonier.ld41.state.spec.PlayState;

public class CirclerEnemy extends Enemy <CirclerEnemyState> {

    public float rotationTimer = 0f;

    public CirclerEnemy (
            int id, PlayState playState,
            Vector2 position
    ) {
        super (id, playState, EnemyType.CIRCLER, position);
    }

    @Override
    public StateMachine<? extends Enemy, CirclerEnemyState> createStateMachine () {
        StateMachine <CirclerEnemy, CirclerEnemyState> stateMachine = new DefaultStateMachine <> (
                this,
                CirclerEnemyState.MOVING
        );

        stateMachine.changeState (CirclerEnemyState.MOVING);

        return stateMachine;
    }
}
