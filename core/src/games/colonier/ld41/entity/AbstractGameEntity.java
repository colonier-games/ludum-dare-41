package games.colonier.ld41.entity;

import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.laser.CircleAbsorber;
import games.colonier.ld41.state.spec.PlayState;

public abstract class AbstractGameEntity implements GameEntity {

    public final int id;
    public Vector2 position = new Vector2 ();
    public float health;
    public final float maxHealth;
    public CircleAbsorber absorber = new CircleAbsorber (
            new Vector2 (),
            0f
    );

    public final PlayState playState;

    public AbstractGameEntity (int id, float maxHealth, PlayState playState) {
        this.id = id;
        this.maxHealth = maxHealth;
        this.health = maxHealth;
        this.playState = playState;
    }

    @Override
    public int getId () {
        return id;
    }

    @Override
    public Vector2 getPosition () {
        return position;
    }

    public void setPosition (Vector2 position) {
        this.position = position;
    }

    @Override
    public float getHealth () {
        return health;
    }

    @Override
    public void setHealth (float health) {
        this.health = health;
    }

    @Override
    public CircleAbsorber getAbsorber () {
        return absorber;
    }

    public void setAbsorber (CircleAbsorber absorber) {
        this.absorber = absorber;
    }
}
