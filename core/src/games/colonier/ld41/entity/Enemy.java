package games.colonier.ld41.entity;

import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.config.DifficultyConfig;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.entity.enemy.EnemyType;
import games.colonier.ld41.laser.Laser;
import games.colonier.ld41.map.GameMapGraphNode;
import games.colonier.ld41.state.spec.PlayState;

public abstract class Enemy <EnemyState extends State <? extends Enemy>> extends AbstractGameEntity {

    public Vector2 velocity = new Vector2 ();

    public float rotation;
    public Laser laser;

    public GraphPath <GameMapGraphNode> currentPath;
    public int currentPathIndex = 0;
    public float pathTimer = 0f;

    public final EnemyType enemyType;

    private EnemyState state;

    private StateMachine <? extends Enemy, EnemyState> stateMachine;

    public abstract StateMachine <? extends Enemy, EnemyState> createStateMachine ();

    public StateMachine <? extends Enemy, EnemyState> getStateMachine () {
        return stateMachine;
    }

    public void setStateMachine (
            StateMachine <? extends Enemy, EnemyState> stateMachine
    ) {
        this.stateMachine = stateMachine;
    }

    public Enemy (int id, PlayState playState, EnemyType enemyType, Vector2 position) {
        super (id, DifficultyConfig.getInstance ().getDifficulty ().enemyHealth, playState);
        this.enemyType = enemyType;

        this.position.set (position);

        this.absorber
                .setRadius (Numbers.TILE_SCALE / 2f - 2f);

        laser = new Laser (
                new Vector2 (),
                new Vector2 (1f, 0f),
                10000f
        );

    }

}
