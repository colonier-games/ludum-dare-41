package games.colonier.ld41.entity;

import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.laser.CircleAbsorber;

public interface GameEntity {

    int getId ();

    Vector2 getPosition ();
    float getHealth ();
    void setHealth (float h);

    CircleAbsorber getAbsorber ();

}
