package games.colonier.ld41.state;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.controller.GameController;
import games.colonier.ld41.controller.GameRenderer;

public interface GameState extends GameApp {

    GameApp getGameApp ();
    SpriteBatch getSpriteBatch ();
    ShapeRenderer getShapeRenderer ();
    OrthographicCamera getGameCamera ();
    OrthographicCamera getGuiCamera ();
    InputMultiplexer getInputMultiplexer ();
    float getDelta ();
    float getStateTimer ();
    Vector2 getMouseScreenCoords ();
    Vector2 getMouseWorldCoords ();

    GameController getController (String name);
    GameRenderer getRenderer (String name);

    void tick ();
    void render ();
    void init ();

}
