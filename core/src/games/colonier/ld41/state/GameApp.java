package games.colonier.ld41.state;

import com.badlogic.gdx.utils.Disposable;

public interface GameApp extends Disposable {

    float getAppTimer ();
    float getLastFrameTime ();
    int getFrameCount ();
    int getFps ();

    float getLastRenderTime ();
    float getLastUpdateTime ();

    GameState getCurrentState ();
    void changeState (Class <? extends GameState> newStateClass);

}
