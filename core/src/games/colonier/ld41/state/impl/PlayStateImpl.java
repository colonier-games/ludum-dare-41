package games.colonier.ld41.state.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.controllers.Controllers;
import games.colonier.ld41.config.EntityConfig;
import games.colonier.ld41.config.InputConfig;
import games.colonier.ld41.config.ScoresConfig;
import games.colonier.ld41.constants.Musics;
import games.colonier.ld41.constants.Strings;
import games.colonier.ld41.entity.Player;
import games.colonier.ld41.game.World;
import games.colonier.ld41.input.GamepadPlayerInputHandler;
import games.colonier.ld41.input.KeyboardAndMousePlayerInputHandler;
import games.colonier.ld41.input.PlayerInputHandler;
import games.colonier.ld41.map.GameMapGraphNode;
import games.colonier.ld41.state.AbstractGameState;
import games.colonier.ld41.state.GameApp;
import games.colonier.ld41.state.spec.PlayState;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlayStateImpl extends AbstractGameState implements PlayState {

    public World world;

    private List<Player> gamePlayers = new ArrayList<> ();

    List <PlayerInputHandler> playerInputHandlers = new ArrayList <> ();

    private IndexedAStarPathFinder <GameMapGraphNode> pathFinder;

    public PlayStateImpl (Reflections reflections, GameApp gameApp) {
        super (reflections, gameApp);
    }

    @Override
    protected String getName () {
        return Strings.GAME_STATE_PLAY;
    }

    @Override
    protected void preInit () {
        super.preInit ();

        ScoresConfig.getInstance ().currentScore = 0;

        if (Musics.CALM_MUSIC ().isPlaying ()) {
            Musics.CALM_MUSIC ().stop ();
        }

        Musics.BATTLE_MUSIC ().setLooping (true);
        Musics.BATTLE_MUSIC ().setVolume (0.5f);
        Musics.BATTLE_MUSIC ().play ();

        world = new World ();

        world.gameMap
                .borderAbsorber ();

        gamePlayers.add (
                new Player (
                        EntityConfig.getInstance ().newId (),
                        this,
                        world.gameMap.playerSpawnPoint.cpy ()
                )
        );

        world.players.addAll (gamePlayers);
        world.entities.addAll (gamePlayers);

        if (InputConfig.getInstance ()
                .isUseJoystick ()) {

            playerInputHandlers.add (
                    new GamepadPlayerInputHandler (
                            this,
                            gamePlayers.get (0),
                            Arrays.asList (
                                    Controllers.getControllers ().toArray ()
                            ).stream ().filter (
                                    c -> c.getName ().equals (InputConfig.getInstance ().getSelectedJoystickName ())
                            )
                            .findFirst ().get ()
                    )
            );

        } else {

            playerInputHandlers.add (
                    new KeyboardAndMousePlayerInputHandler (
                            this,
                            gamePlayers.get (0)
                    )
            );

        }

        pathFinder = new IndexedAStarPathFinder <> (world.gameMap.gameMapGraph);

    }

    @Override
    protected void initImpl () {



    }

    private void handleInput () {

        playerInputHandlers.stream ()
                .forEach (
                        PlayerInputHandler::handleInput
                );

    }

    @Override
    protected void tickImpl (float delta) {

        handleInput ();

        gameCamera.position.set (
                gamePlayers.get (0).position.x,
                gamePlayers.get (0).position.y,
                0f
        );
        gameCamera.update ();

    }

    @Override
    protected void renderImpl (float delta) {

    }

    @Override
    public World getWorld () {
        return world;
    }

    @Override
    public List <Player> getGamePlayers () {
        return gamePlayers;
    }

    @Override
    public IndexedAStarPathFinder <GameMapGraphNode> getPathFinder () {
        return pathFinder;
    }
}
