package games.colonier.ld41.state.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.MathUtils;
import games.colonier.ld41.config.DifficultyConfig;
import games.colonier.ld41.config.GraphicsConfig;
import games.colonier.ld41.config.InputConfig;
import games.colonier.ld41.constants.Fonts;
import games.colonier.ld41.constants.Musics;
import games.colonier.ld41.constants.Sounds;
import games.colonier.ld41.constants.Strings;
import games.colonier.ld41.menu.Menu;
import games.colonier.ld41.menu.MenuItem;
import games.colonier.ld41.state.AbstractGameState;
import games.colonier.ld41.state.GameApp;
import games.colonier.ld41.state.spec.MainMenuState;
import org.reflections.Reflections;

import java.util.stream.Collectors;

public class MainMenuStateImpl extends AbstractGameState implements MainMenuState {

    float hueShift = 0f;
    private Menu rootMenu;
    private Menu currentMenu;
    private MenuItem activeMenuItem;
    private int activeMenuItemIndex = 0;

    public MainMenuStateImpl (Reflections reflections, GameApp gameApp) {
        super (reflections, gameApp);
    }

    @Override
    protected String getName () {
        return Strings.GAME_STATE_MAIN_MENU;
    }

    @Override
    protected void initImpl () {

        if (Musics.BATTLE_MUSIC ()
                .isPlaying ()) {
            Musics.BATTLE_MUSIC ()
                    .stop ();
        }

        Musics.CALM_MUSIC ()
                .setLooping (true);
        Musics.CALM_MUSIC ()
                .play ();

        rootMenu = Menu.create (
                "root",
                "Lazors",
                Menu.MenuItems.of (
                        MenuItem.create (
                                "play-button",
                                "Play",
                                (mi) -> {
                                    // changeState (PlayStateImpl.class);
                                    setCurrentMenu (rootMenu.findMenu ("difficulties"));
                                }
                        ),
                        MenuItem.create (
                                "options-button",
                                "Options",
                                (mi) -> {
                                    setCurrentMenu (rootMenu.findMenu ("options"));
                                }
                        ),
                        MenuItem.create (
                                "exit-button",
                                "Exit",
                                (mi) -> {
                                    System.exit (0);
                                }
                        )
                ),
                Menu.Menus.of (
                        Menu.create (
                                "difficulties",
                                "Choose difficulty",
                                Menu.MenuItems.of (
                                        MenuItem.create (
                                                "easy-difficulty",
                                                "Easy",
                                                (mi) -> {

                                                    DifficultyConfig.getInstance ()
                                                            .setDifficulty (DifficultyConfig.Difficulty.EASY);

                                                    changeState (PlayStateImpl.class);

                                                }
                                        ),
                                        MenuItem.create (
                                                "normal-difficulty",
                                                "Normal",
                                                (mi) -> {

                                                    DifficultyConfig.getInstance ()
                                                            .setDifficulty (DifficultyConfig.Difficulty.NORMAL);

                                                    changeState (PlayStateImpl.class);

                                                }
                                        ),
                                        MenuItem.create (
                                                "hard-difficulty",
                                                "Hard",
                                                (mi) -> {

                                                    DifficultyConfig.getInstance ()
                                                            .setDifficulty (DifficultyConfig.Difficulty.HARD);

                                                    changeState (PlayStateImpl.class);

                                                }
                                        ),
                                        MenuItem.create (
                                                "brutal-difficulty",
                                                "Brutal",
                                                (mi) -> {

                                                    DifficultyConfig.getInstance ()
                                                            .setDifficulty (DifficultyConfig.Difficulty.BRUTAL);

                                                    changeState (PlayStateImpl.class);

                                                }
                                        )
                                ),
                                null
                        ),
                        Menu.create (
                                "options",
                                "Options",
                                Menu.MenuItems.of (
                                        MenuItem.create (
                                                "back-button",
                                                "Back",
                                                (mi) -> { setCurrentMenu (rootMenu.findMenu ("root")); }
                                        ),
                                        MenuItem.create (
                                                "keyboard-mouse-switch-button",
                                                InputConfig.getInstance ()
                                                        .isUseJoystick () ? "Joystick" : "Keyboard / mouse",
                                                (mi) -> {

                                                    InputConfig.getInstance ()
                                                            .setUseJoystick (
                                                                    !InputConfig.getInstance ()
                                                                            .isUseJoystick ()
                                                            );

                                                    mi.setText (
                                                            InputConfig.getInstance ()
                                                                    .isUseJoystick () ? "Joystick" : "Keyboard / mouse"
                                                    );
                                                }
                                        )
                                                .rendered (mi -> InputConfig.getInstance ()
                                                        .isJoystickAvailable ()),
                                        MenuItem.create (
                                                "joystick-list-button",
                                                "Joysticks",
                                                (mi) -> { setCurrentMenu (rootMenu.findMenu ("joystick-list")); }
                                        )
                                                .rendered ((mi) -> InputConfig.getInstance ()
                                                        .isUseJoystick ()),
                                        MenuItem.create (
                                                "fullscreen-windowed-button",
                                                GraphicsConfig.getInstance ()
                                                        .isFullScreen () ? "Fullscreen" : "Windowed",
                                                (mi) -> {

                                                    GraphicsConfig.getInstance ()
                                                            .setFullScreen (
                                                                    !GraphicsConfig.getInstance ()
                                                                            .isFullScreen ()
                                                            );

                                                    mi.setText (
                                                            GraphicsConfig.getInstance ()
                                                                    .isFullScreen () ? "Fullscreen" : "Windowed"
                                                    );
                                                }
                                        )
                                ),
                                Menu.Menus.of (
                                        Menu.create (
                                                "joystick-list",
                                                "Select gamepad",
                                                Menu.MenuItems.of (
                                                        InputConfig.getInstance ()
                                                                .getAvailableJoysticks ()
                                                                .stream ()
                                                                .map (
                                                                        name -> MenuItem.create (
                                                                                "joystick-" + name + "-button",
                                                                                name,
                                                                                menuItem -> InputConfig.getInstance ()
                                                                                        .setSelectedJoystickName (name)
                                                                        )
                                                                                .color (
                                                                                        mainMenuState -> InputConfig.getInstance ()
                                                                                                .getSelectedJoystickName ()
                                                                                                .equals (name)
                                                                                                ? Color.MAGENTA
                                                                                                :
                                                                                                mainMenuState.getActiveMenuItem ()
                                                                                                        .getId ()
                                                                                                        .equals (
                                                                                                                "joystick-" + name + "-button"
                                                                                                        )
                                                                                                        ? Color.BLUE
                                                                                                        : Color.RED
                                                                                )
                                                                )
                                                                .collect (Collectors.toList ())
                                                ),
                                                null
                                        )
                                )
                        )
                )
        );

        currentMenu = rootMenu;
        activeMenuItem = currentMenu.getMenuItems ()
                .get (0);

    }

    @Override
    protected void tickImpl (float delta) {

    }

    Color color = new Color ();
    GlyphLayout glyphLayout = new GlyphLayout ();

    @Override
    protected void renderImpl (float delta) {

        glyphLayout.setText (
                Fonts.bigBold (),
                currentMenu.getTitle ()
        );

        spriteBatch.setProjectionMatrix (getGuiCamera ().combined);

        spriteBatch.begin ();

        for (int i = 0; i < currentMenu.getTitle ().length (); i++) {

            float angle = hueShift + i * 30f;

            angle -= ((int) ( angle / 360f ) ) * 360f;

            color.fromHsv (
                    angle,
                    0.75f,
                    0.75f
            );
            color.a = 1f;

            Fonts.bigBold ()
                    .draw (
                            spriteBatch,
                            "[#" + color.toString () + "]" + currentMenu.getTitle ().charAt (i),
                            -Gdx.graphics.getWidth () / 4f + (i + 1) * (Gdx.graphics.getWidth () / 2f / ( currentMenu.getTitle ().length () + 1 )),
                            Gdx.graphics.getHeight () / 4f + MathUtils.sinDeg (angle) * 40f
                    );

        }

        spriteBatch.end ();

        hueShift += 60f * delta;

    }

    @Override
    public boolean keyDown (int keycode) {

        if (keycode == Input.Keys.UP) {

            do {

                activeMenuItemIndex--;
                if (activeMenuItemIndex < 0) {
                    activeMenuItemIndex = currentMenu.getMenuItems ()
                            .size () - 1;
                }

                if (activeMenuItemIndex >= currentMenu.getMenuItems ()
                        .size ()) {
                    activeMenuItemIndex = 0;
                }

                setActiveMenuItem (currentMenu.getMenuItems ()
                        .get (activeMenuItemIndex));

            } while (!getActiveMenuItem ().getIsRenderedPredicate ()
                    .test (getActiveMenuItem ()));

            Sounds.SELECT ()
                    .play ();

        }

        if (keycode == Input.Keys.DOWN) {

            do {

                activeMenuItemIndex++;
                if (activeMenuItemIndex < 0) {
                    activeMenuItemIndex = currentMenu.getMenuItems ()
                            .size () - 1;
                }

                if (activeMenuItemIndex >= currentMenu.getMenuItems ()
                        .size ()) {
                    activeMenuItemIndex = 0;
                }

                setActiveMenuItem (currentMenu.getMenuItems ()
                        .get (activeMenuItemIndex));

            } while (!getActiveMenuItem ().getIsRenderedPredicate ()
                    .test (getActiveMenuItem ()));

            Sounds.SELECT ()
                    .play ();

        }

        if (keycode == Input.Keys.ENTER
                || keycode == Input.Keys.SPACE) {

            if (activeMenuItem.getSelectionCallback () != null) {

                activeMenuItem.getSelectionCallback ()
                        .onSelected (activeMenuItem);

                Sounds.SELECT ()
                        .play ();

            }

        }

        if (keycode == Input.Keys.ESCAPE) {

            if (getCurrentMenu ().getParentMenu () != null) {

                setCurrentMenu (
                        getCurrentMenu ().getParentMenu ()
                );

                Sounds.SELECT ()
                        .play ();

            }

        }

        return false;

    }

    @Override
    public Menu getRootMenu () {
        return rootMenu;
    }

    @Override
    public Menu getCurrentMenu () {
        return currentMenu;
    }

    @Override
    public void setCurrentMenu (Menu currentMenu) {
        this.currentMenu = currentMenu;
        activeMenuItemIndex = 0;
        setActiveMenuItem (currentMenu.getMenuItems ()
                .get (activeMenuItemIndex));
    }

    @Override
    public MenuItem getActiveMenuItem () {
        return activeMenuItem;
    }

    public void setActiveMenuItem (MenuItem activeMenuItem) {
        this.activeMenuItem = activeMenuItem;
    }

}
