package games.colonier.ld41.state.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import games.colonier.ld41.config.ScoresConfig;
import games.colonier.ld41.constants.Fonts;
import games.colonier.ld41.state.AbstractGameState;
import games.colonier.ld41.state.GameApp;
import org.reflections.Reflections;

public class GameOverStateImpl extends AbstractGameState {

    GlyphLayout glyphLayout = new GlyphLayout ();

    public GameOverStateImpl (Reflections reflections, GameApp gameApp) {
        super (reflections, gameApp);
    }

    @Override
    protected String getName () {
        return "GameOverState";
    }

    @Override
    protected void initImpl () {

    }

    @Override
    protected void tickImpl (float delta) {

    }

    @Override
    protected void renderImpl (float delta) {

        spriteBatch.setProjectionMatrix (guiCamera.combined);

        spriteBatch.begin ();

        glyphLayout.setText (
                Fonts.bigItalic (),
                "Game over"
        );

        Fonts.bigItalic ().setColor (Color.BLACK);

        Fonts.bigItalic ().draw (
                spriteBatch,
                "Game over",
                -glyphLayout.width / 2f,
                Gdx.graphics.getHeight () / 4f
        );

        glyphLayout.setText (
                Fonts.bigBold (),
                "You scored " + ScoresConfig.getInstance ().currentScore
        );

        Fonts.bigBold ().draw (
                spriteBatch,
                "You scored [#3388DD]" + ScoresConfig.getInstance ().currentScore,
                -glyphLayout.width / 2f,
                -glyphLayout.height / 2f
        );

        if (ScoresConfig.getInstance ().currentScore > ScoresConfig.getInstance ().highScore) {

            glyphLayout.setText (
                    Fonts.smallBold (),
                    "New highscore!"
            );

            Fonts.smallBold ().setColor (Color.RED);

            Fonts.smallBold ()
                    .draw (
                            spriteBatch,
                            "New highscore!",
                            -glyphLayout.width / 2f,
                            -Gdx.graphics.getHeight () / 4f
                    );

        } else if (ScoresConfig.getInstance ().currentScore == ScoresConfig.getInstance ().highScore) {

            glyphLayout.setText (
                    Fonts.smallBold (),
                    "This is the highscore"
            );

            Fonts.smallBold ().setColor (Color.GRAY);

            Fonts.smallBold ()
                    .draw (
                            spriteBatch,
                            "This is the highscore",
                            -glyphLayout.width / 2f,
                            -Gdx.graphics.getHeight () / 4f
                    );

        } else {


            glyphLayout.setText (
                    Fonts.smallBold (),
                    "Highscore: " + ScoresConfig.getInstance ().highScore
            );

            Fonts.smallBold ().setColor (Color.BLACK);

            Fonts.smallBold ()
                    .draw (
                            spriteBatch,
                            "Highscore: [#DD6666]" + ScoresConfig.getInstance ().highScore,
                            -glyphLayout.width / 2f,
                            -Gdx.graphics.getHeight () / 4f
                    );

        }

        spriteBatch.end ();

    }

    @Override
    public boolean keyDown (int keycode) {

        if (ScoresConfig.getInstance ().currentScore > ScoresConfig.getInstance ().highScore) {
            ScoresConfig.getInstance ().highScore = ScoresConfig.getInstance ().currentScore;
        }

        changeState (MainMenuStateImpl.class);

        return false;
    }
}
