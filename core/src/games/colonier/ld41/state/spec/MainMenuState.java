package games.colonier.ld41.state.spec;

import games.colonier.ld41.menu.Menu;
import games.colonier.ld41.menu.MenuItem;
import games.colonier.ld41.state.GameState;

public interface MainMenuState extends GameState {

    Menu getRootMenu ();

    Menu getCurrentMenu ();
    MenuItem getActiveMenuItem ();

    void setCurrentMenu (Menu newCurrentMenu);
    void setActiveMenuItem (MenuItem newActiveMenuItem);



}
