package games.colonier.ld41.state.spec;

import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import games.colonier.ld41.entity.Player;
import games.colonier.ld41.game.World;
import games.colonier.ld41.map.GameMapGraphNode;
import games.colonier.ld41.state.GameState;

import java.util.List;

public interface PlayState extends GameState {

    World getWorld ();

    List<Player> getGamePlayers ();

    IndexedAStarPathFinder <GameMapGraphNode> getPathFinder ();

}
