package games.colonier.ld41.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Disposable;
import games.colonier.ld41.annotation.Controller;
import games.colonier.ld41.annotation.Renderer;
import games.colonier.ld41.state.spec.PlayState;
import games.colonier.ld41.controller.GameController;
import games.colonier.ld41.controller.GameRenderer;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractGameState implements InputProcessor, GameState, Disposable {

    private final Reflections reflections;
    protected final GameApp gameApp;

    protected SpriteBatch spriteBatch;
    protected ShapeRenderer shapeRenderer;

    protected OrthographicCamera gameCamera;
    protected OrthographicCamera guiCamera;
    private OrthographicCamera nullCamera;
    protected InputMultiplexer inputMultiplexer;

    protected List <GameController> orderedControllers = new ArrayList <> ();
    protected List <GameRenderer> orderedRenderers = new ArrayList <> ();

    protected Map <String, GameController> controllersByName = new HashMap <> ();
    protected Map <String, GameRenderer> renderersByName = new HashMap <> ();

    protected Vector2 mouseScreenCoords = new Vector2 ();
    protected Vector2 mouseWorldCoords = new Vector2 ();

    protected float delta;
    protected float stateTimer;

    private FrameBuffer frameBuffer;

    @Override
    public void dispose () {

        spriteBatch.dispose ();
        shapeRenderer.dispose ();

        frameBuffer.dispose ();

        orderedRenderers.clear ();
        orderedRenderers.clear ();
        controllersByName.clear ();
        renderersByName.clear ();

        System.gc ();

    }

    protected void disposeImpl () {}

    public AbstractGameState (Reflections reflections, GameApp gameApp) {
        this.reflections = reflections;
        this.gameApp = gameApp;
    }

    protected abstract String getName ();

    public void init () {

        spriteBatch = new SpriteBatch ();
        shapeRenderer = new ShapeRenderer ();

        gameCamera = new OrthographicCamera (
                Gdx.graphics.getWidth (),
                Gdx.graphics.getHeight ()
        );
        gameCamera.position.set (
                -Gdx.graphics.getWidth () / 2f,
                Gdx.graphics.getHeight () / 2f,
                0f
        );

        guiCamera = new OrthographicCamera (
                Gdx.graphics.getWidth (),
                Gdx.graphics.getHeight ()
        );

        nullCamera = new OrthographicCamera (
                Gdx.graphics.getWidth (),
                Gdx.graphics.getHeight ()
        );
        nullCamera.setToOrtho (true);

        preInit ();

        scanForControllersAndRenderers ();
        initializeControllersAndRenderers ();

        inputMultiplexer = new InputMultiplexer (this);

        Gdx.input.setInputProcessor (
                inputMultiplexer
        );

        initImpl ();

        frameBuffer = new FrameBuffer (
                Pixmap.Format.RGBA8888,
                Gdx.graphics.getWidth (),
                Gdx.graphics.getHeight (),
                false
        );

    }

    protected void preInit () {

    }

    protected abstract void initImpl ();

    private void findMouseCoords () {

        int screenX = Gdx.input.getX ();
        int screenY = Gdx.input.getY ();

        Ray pickRay = gameCamera.getPickRay (screenX, screenY);

        mouseWorldCoords.set (
                pickRay.origin.x,
                pickRay.origin.y
        );

        mouseScreenCoords.set (
                screenX,
                screenY
        );

    }

    public void tick () {

        preTick ();

        findMouseCoords ();

        this.delta = Gdx.graphics.getDeltaTime ();

        this.stateTimer += delta;

        for (GameController controller : orderedControllers) {

            controller.update (
                    this.delta,
                    this
            );

        }

        tickImpl (delta);

    }

    public void render () {

        frameBuffer.begin ();

        Gdx.gl.glClearColor (0f, 0f, 0f, 0f);
        Gdx.gl.glClear (Gdx.gl.GL_COLOR_BUFFER_BIT);

        preRender ();

        for (GameRenderer renderer : orderedRenderers) {

            renderer.render (
                    this.delta,
                    this
            );

        }

        renderImpl (this.delta);

        frameBuffer.end ();

        postProcess ();

    }

    protected void postProcess () {

        spriteBatch.setProjectionMatrix (nullCamera.combined);

        spriteBatch.begin ();

        spriteBatch.draw (
                frameBuffer.getColorBufferTexture (),
                0, 0,
                Gdx.graphics.getWidth (),
                Gdx.graphics.getHeight ()
        );

        spriteBatch.end ();

    }

    protected void preTick () {

    }

    protected abstract void tickImpl (float delta);

    protected void preRender () {

    }

    protected abstract void renderImpl (float delta);

    private void initializeControllersAndRenderers () {

        Set <Object> initializedControllersAndRenderers = new HashSet <> ();

        for (GameController controller : orderedControllers) {

            if (initializedControllersAndRenderers.add (controller)) {
                controller.init ();
            }

        }

        for (GameRenderer renderer : orderedRenderers) {

            if (initializedControllersAndRenderers.add (renderer)) {
                renderer.init ();
            }

        }

    }

    private void scanForControllersAndRenderers () {

        try {

            Set<Class <?>> controllerClasses =
                    reflections.getTypesAnnotatedWith (Controller.class);

            Set <Class <?>> rendererClasses =
                    reflections.getTypesAnnotatedWith (Renderer.class);

            Set <Class <?>> controllerAndRendererClasses =
                    new HashSet <> ();

            for (Class <?> clazz : controllerClasses) {

                if (clazz.isAnnotationPresent (Controller.class) && clazz.isAnnotationPresent (Renderer.class)) {
                    controllerAndRendererClasses.add (clazz);
                }

            }

            controllerClasses.removeAll (controllerAndRendererClasses);
            rendererClasses.removeAll (controllerAndRendererClasses);

            controllerClasses.removeAll (
                    controllerClasses.stream ()
                    .filter (
                            c -> !Arrays.asList (
                                    c.getAnnotation (Controller.class)
                                    .gameStates ()
                            ).contains (getName ())
                    ).collect(Collectors.toList())
            );

            rendererClasses.removeAll (
                    controllerClasses.stream ()
                            .filter (
                                    c -> !Arrays.asList (
                                            c.getAnnotation (Controller.class)
                                                    .gameStates ()
                                    ).contains (getName ())
                            ).collect(Collectors.toList())
            );

            controllerAndRendererClasses.removeAll (
                    controllerClasses.stream ()
                            .filter (
                                    c -> !Arrays.asList (
                                            c.getAnnotation (Controller.class)
                                                    .gameStates ()
                                    ).contains (getName ())
                            ).collect(Collectors.toList())
            );

            List <GameController> controllerList = new ArrayList <> ();
            List <GameRenderer> rendererList = new ArrayList <> ();

            for (Class <?> controllerClass : controllerClasses) {

                ReflectionUtils.getConstructors (
                        controllerClass,
                        ReflectionUtils.withParametersAssignableTo (getClass ())
                ).stream ().findFirst ()
                        .ifPresent (
                                constructor -> {

                                    try {

                                        GameController controller = (GameController) constructor.newInstance (
                                                this
                                        );

                                        controllersByName.put (
                                                controller.getControllerName (),
                                                controller
                                        );

                                        controllerList.add (controller);

                                    } catch (Exception exc) {

                                        throw new RuntimeException (
                                                "Couldn't initialize controller of class " + controllerClass
                                                        + ", reason: ",
                                                exc
                                        );

                                    }

                                }
                        );

            }

            for (Class <?> rendererClass : rendererClasses) {

                ReflectionUtils.getConstructors (
                        rendererClass,
                        ReflectionUtils.withParametersAssignableTo (getClass ())
                ).stream ().findFirst ()
                        .ifPresent (
                                constructor -> {

                                    try {

                                        GameRenderer renderer = (GameRenderer) constructor.newInstance (
                                                this
                                        );

                                        renderersByName.put (
                                                renderer.getRendererName (),
                                                renderer
                                        );

                                        rendererList.add (renderer);

                                    } catch (Exception exc) {

                                        throw new RuntimeException (
                                                "Couldn't initialize renderer of class " + rendererClass
                                                + ", reason: ",
                                                exc
                                        );

                                    }

                                }
                        );

            }

            for (Class <?> bothClass : controllerAndRendererClasses) {

                ReflectionUtils.getConstructors (
                        bothClass,
                        ReflectionUtils.withParametersAssignableTo (getClass ())
                ).stream ().findFirst ()
                        .ifPresent (
                                constructor -> {

                                    try {

                                        Object object = constructor.newInstance (
                                                this
                                        );

                                        controllersByName.put (
                                                ( (GameController) object ).getControllerName (),
                                                (GameController) object
                                        );
                                        controllerList.add ((GameController) object);

                                        renderersByName.put (
                                                ( (GameRenderer) object ).getRendererName (),
                                                (GameRenderer) object
                                        );
                                        rendererList.add ((GameRenderer) object);

                                    } catch (Exception exc) {

                                        throw new RuntimeException (
                                                "Couldn't initialize controller and renderer of class " + bothClass
                                                        + ", reason: ",
                                                exc
                                        );

                                    }

                                }
                        );

            }

            orderedControllers.addAll (
                    controllerList.stream ()
                            .sorted (
                                    Comparator.comparingInt (c -> c.getClass ()
                                            .getAnnotation (
                                                    Controller.class
                                            )
                                            .order ())
                            )
                            .collect (Collectors.toList ())
            );

            orderedRenderers.addAll (
                    rendererList.stream ()
                            .sorted (
                                    Comparator.comparingInt (
                                            c -> c.getClass ()
                                                    .getAnnotation (
                                                            Renderer.class
                                                    )
                                                    .order ()
                                    )
                            )
                            .collect (Collectors.toList ())
            );

            orderedControllers.stream ()
                    .forEach (System.out::println);
            orderedRenderers.stream ()
                    .forEach (System.out::println);

        } catch (Exception exc) {

            throw new RuntimeException (
                    "Couldn't scan for and initialize game controllers and renderers! Reason: ",
                    exc
            );

        }

    }

    @Override
    public boolean keyDown (int keycode) {
        return false;
    }

    @Override
    public boolean keyUp (int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped (char character) {
        return false;
    }

    @Override
    public boolean touchDown (int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp (int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged (int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved (int screenX, int screenY) {



        return false;
    }

    @Override
    public boolean scrolled (int amount) {
        return false;
    }

    public float getAppTimer () {
        return gameApp.getAppTimer ();
    }

    public float getLastFrameTime () {
        return gameApp.getLastFrameTime ();
    }

    public int getFrameCount () {
        return gameApp.getFrameCount ();
    }

    public int getFps () {
        return gameApp.getFps ();
    }

    public float getLastRenderTime () {
        return gameApp.getLastRenderTime ();
    }

    public float getLastUpdateTime () {
        return gameApp.getLastUpdateTime ();
    }

    @Override
    public GameApp getGameApp () {
        return gameApp;
    }

    @Override
    public SpriteBatch getSpriteBatch () {
        return spriteBatch;
    }

    @Override
    public ShapeRenderer getShapeRenderer () {
        return shapeRenderer;
    }

    @Override
    public OrthographicCamera getGameCamera () {
        return gameCamera;
    }

    @Override
    public OrthographicCamera getGuiCamera () {
        return guiCamera;
    }

    @Override
    public InputMultiplexer getInputMultiplexer () {
        return inputMultiplexer;
    }

    @Override
    public Vector2 getMouseScreenCoords () {
        return mouseScreenCoords;
    }

    @Override
    public Vector2 getMouseWorldCoords () {
        return mouseWorldCoords;
    }

    @Override
    public float getDelta () {
        return delta;
    }

    @Override
    public float getStateTimer () {
        return stateTimer;
    }

    @Override
    public GameController getController (String name) {
        return controllersByName.get (name);
    }

    @Override
    public GameRenderer getRenderer (String name) {
        return renderersByName.get (name);
    }

    @Override
    public GameState getCurrentState () {
        return gameApp.getCurrentState ();
    }

    @Override
    public void changeState (Class <? extends GameState> newStateClass) {
        gameApp.changeState (newStateClass);
    }
}
