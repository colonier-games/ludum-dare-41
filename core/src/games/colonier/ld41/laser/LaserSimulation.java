package games.colonier.ld41.laser;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class LaserSimulation implements Serializable {

    float errorThreshold = 1f;

    Vector2 intersectionPoint = new Vector2 ();

    Map <Laser, List <Beam>> beamMap = new HashMap <> ();

    Pool <Beam> beamPool = new Pool <Beam> () {
        @Override
        protected Beam newObject () {
            return new Beam (new Vector2 (), new Vector2 ());
        }
    };

    Pool <BeamIntersectionResult> beamIntersectionResultPool = new Pool <BeamIntersectionResult> () {
        @Override
        protected BeamIntersectionResult newObject () {
            return new BeamIntersectionResult ();
        }
    };

    Vector2 tmp = new Vector2 ();

    final static class BeamIntersectionResult implements Serializable, Pool.Poolable {

        public boolean intersects = false;
        public boolean isAbsorbed = false;
        public Vector2 intersectionPoint = new Vector2 ();
        public Beam beam;
        public Reflector reflector;
        public Absorber absorber;

        @Override
        public String toString () {
            return "BeamIntersectionResult{" +
                    "intersects=" + intersects +
                    ", isAbsorbed=" + isAbsorbed +
                    ", intersectionPoint=" + intersectionPoint +
                    ", beam=" + beam +
                    ", reflector=" + reflector +
                    ", absorber=" + absorber +
                    '}';
        }

        @Override
        public void reset () {

            intersects = false;
            isAbsorbed = false;
            intersectionPoint.set (0.0f, 0.0f);
            beam = null;
            reflector = null;
            absorber = null;

        }
    }

    private BeamIntersectionResult intersectBeamWithAbsorber (
            Beam beam,
            Absorber absorber
    ) {

        BeamIntersectionResult result = beamIntersectionResultPool.obtain ();

        result.beam = beam;
        result.absorber = absorber;

        if (absorber.intersectsWithBeam (beam)) {

            result.intersects = true;
            result.isAbsorbed = true;
            result.intersectionPoint.set (absorber.getBeamCutPoint (beam));

        }

        return result;

    }

    private BeamIntersectionResult intersectBeamWithReflector (
            Beam beam,
            Reflector reflector
    ) {

        BeamIntersectionResult result = beamIntersectionResultPool.obtain ();

        result.beam = beam;
        result.reflector = reflector;
        result.isAbsorbed = false;

        if (
                Intersector.intersectSegments (
                        beam.getStart (),
                        beam.getEnd (),
                        reflector.getLineSegmentStart (),
                        reflector.getLineSegmentEnd (),
                        intersectionPoint
                )
                ) {

            if (intersectionPoint.dst (reflector.getPosition ()) <= reflector.getWidth ()
                    && intersectionPoint.dst (beam.getStart ()) > errorThreshold) {

                result.intersects = true;
                result.intersectionPoint.set (intersectionPoint);

            }

        }

        return result;

    }

    Set <Laser> laserKeysToRemove = new HashSet <> ();
    Set <Laser> laserKeysToAdd = new HashSet <> ();

    List <BeamIntersectionResult> intersectionResults = new ArrayList <> ();

    Vector2 beamDir = new Vector2 ();
    Vector2 closerNormal = new Vector2 ();

    public List <Beam> simulate (
            List <Laser> lasers,
            List <Reflector> reflectors,
            List <Absorber> absorbers
    ) {

        reflectors.stream ().forEach (Reflector::clearHittingBeams);
        reflectors.stream ().forEach (Reflector::clearOriginatingBeams);
        absorbers.stream ().forEach (Absorber::clearHittingBeams);

        List <Beam> resultList = new ArrayList <> ();

        // Create initial beams

        beamMap.values ()
                .stream ()
                .forEach (
                        list -> list.stream ()
                                .forEach (beamPool::free)
                );

        int numFreedBeams = beamMap.values ()
                .stream ()
                .mapToInt (list -> list.size ())
                .sum ();

        laserKeysToRemove.clear ();
        laserKeysToAdd.clear ();

        beamMap.values ()
                .stream ()
                .forEach (
                        list -> list.clear ()
                );

        laserKeysToRemove.addAll (
                beamMap.keySet ()
                        .stream ()
                        .filter (laser -> !lasers.contains (laser))
                        .collect (Collectors.toList ()));

        laserKeysToRemove.stream ().forEach (k -> beamMap.remove (k));

        laserKeysToAdd.addAll (
                lasers.stream ()
                .filter (laser -> !beamMap.containsKey (laser))
                .collect(Collectors.toList())
        );

        laserKeysToAdd.stream ().forEach (
                k -> beamMap.put (k, new ArrayList <> ())
        );

        for (Laser laser : lasers) {

            Beam beam = beamPool.obtain ();

            beam.setStart (laser.getPosition ());
            beam.setEnd (laser.getPosition ());
            beam.getEnd ()
                    .add (
                            tmp.set (laser.getDirection ())
                                    .scl (laser.getLength ())
                    );

            beamMap.get (laser)
                    .add (beam);

            laser.getOriginatingBeams ().clear ();
            laser.addHittingBeam (beam);
            beam.setBeamOrigin (laser);

        }

        boolean changed = false;

        int round = 0;

        do {

            ++round;
            changed = false;

            for (Map.Entry <Laser, List <Beam>> entry : beamMap.entrySet ()) {

                Laser laser = entry.getKey ();
                List <Beam> beams = entry.getValue ();

                Beam lastBeam = beams.get (beams.size () - 1);

                intersectionResults.stream ()
                        .forEach (beamIntersectionResultPool::free);

                intersectionResults.clear ();

                for (Absorber absorber : absorbers) {

                    intersectionResults.add (
                            intersectBeamWithAbsorber (
                                    lastBeam,
                                    absorber
                            )
                    );

                }

                for (Reflector reflector : reflectors) {

                    intersectionResults.add (
                            intersectBeamWithReflector (
                                    lastBeam,
                                    reflector
                            )
                    );

                }

                Optional <BeamIntersectionResult> closestIntersectionResult = intersectionResults.stream ()
                        .filter (r -> r.intersects)
                        .sorted ((r1, r2) -> Float.compare (
                                r1.intersectionPoint.dst (r1.beam.getStart ()),
                                r2.intersectionPoint.dst (r2.beam.getStart ())
                        ))
                        .findFirst ();

                if (closestIntersectionResult.isPresent ()) {

                    BeamIntersectionResult closestIntersection = closestIntersectionResult.get ();

                    lastBeam.setEnd (closestIntersection.intersectionPoint.cpy ());

                    if (closestIntersection.reflector != null) {
                        closestIntersection.reflector.addHittingBeam (lastBeam);
                    } else if (closestIntersection.absorber != null) {
                        closestIntersection.absorber.addHittingBeam (lastBeam);
                    }

                    if (closestIntersection.isAbsorbed) {

                        continue;

                    }

                    beamDir.set (lastBeam.getStart ())
                            .sub (lastBeam.getEnd ())
                            .nor ();

                    closerNormal.set (closestIntersection.reflector.getNormal ())
                            .cpy ()
                            .nor ();

                    float angle = beamDir.angle (closerNormal);

                    if (angle < -90f || angle > 90f) {

                        /* angle = 90f - ( Math.abs (angle) - 90 );
                        angle += 180.0f; */

                        closerNormal.rotate (180f);
                        angle = beamDir.angle (closerNormal);

                    }

                    float newBeamLength = laser.getLength ()
                            - beams.stream ()
                            .map (Beam::length)
                            .reduce ((p, c) -> p + c)
                            .orElse (0f);

                    Beam newBeam = beamPool.obtain ();

                    newBeam.setStart (closestIntersection.intersectionPoint);
                    newBeam.setEnd (closestIntersection.intersectionPoint);
                    newBeam.getEnd ()
                            .add (
                                    closerNormal.rotate (angle)
                                    .nor ()
                                    .scl (newBeamLength)
                            );

                    entry.getValue ()
                            .add (newBeam);

                    if (closestIntersection.reflector != null) {
                        closestIntersection.reflector.addOriginatingBeam (newBeam);
                        newBeam.setBeamOrigin (closestIntersection.reflector);
                    }

                    changed = true;

                }

            }

            if (round >= 1000) {

                System.out.println ("Too many iterations, skipping ...");
                break;

            }

        } while (changed);

        beamMap.values ()
                .stream ()
                .forEach (resultList::addAll);

        return resultList;

    }

}
