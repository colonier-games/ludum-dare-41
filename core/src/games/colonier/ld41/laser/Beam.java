package games.colonier.ld41.laser;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

import java.io.Serializable;

public class Beam implements Serializable, Pool.Poolable {

    private Vector2 start;
    private Vector2 end;

    private boolean lenCalculated = false;
    private float len = 0.0f;

    private BeamOrigin beamOrigin;
    private BeamHittable hitObject;

    public Beam (Vector2 start, Vector2 end) {
        this.start = start;
        this.end = end;
    }

    public float length () {

        if (!lenCalculated) {

            float dx = start.x - end.x;
            float dy = start.y - end.y;

            len = (float) Math.sqrt (
                    dx * dx + dy * dy
            );

            lenCalculated = true;

        }

        return len;

    }

    @Override
    public String toString () {
        return "Beam{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }

    @Override
    public void reset () {

        start.set (0.0f, 0.0f);
        end.set (0.0f, 0.0f);
        len = 0.0f;
        lenCalculated = false;
        hitObject = null;

    }

    public Vector2 getStart () {
        return start;
    }

    public void setStart (Vector2 start) {
        this.start.set (start);
        lenCalculated = false;
    }

    public Vector2 getEnd () {
        return end;
    }

    public void setEnd (Vector2 end) {
        this.end.set (end);
        lenCalculated = false;
    }

    public BeamHittable getHitObject () {
        return hitObject;
    }

    public void setHitObject (BeamHittable hitObject) {
        this.hitObject = hitObject;
    }

    public BeamOrigin getBeamOrigin () {
        return beamOrigin;
    }

    public void setBeamOrigin (BeamOrigin beamOrigin) {
        this.beamOrigin = beamOrigin;
    }
}
