package games.colonier.ld41.laser;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;

public class CircleAbsorber extends Absorber {

    private Vector2 center;
    private float radius;

    private Vector2 tmp1 = new Vector2 ();
    private Vector2 tmp2 = new Vector2 ();

    public CircleAbsorber (Vector2 center, float radius) {
        this.center = center;
        this.radius = radius;
    }

    public Vector2 getCenter () {
        return center;
    }

    public void setCenter (Vector2 center) {
        this.center = center;
    }

    public float getRadius () {
        return radius;
    }

    public void setRadius (float radius) {
        this.radius = radius;
    }

    @Override
    public boolean intersectsWithBeam (Beam beam) {

        return Intersector.intersectSegmentCircle (
                beam.getStart (),
                beam.getEnd (),
                getCenter (),
                getRadius () * getRadius ()
        );

    }

    @Override
    public Vector2 getBeamCutPoint (Beam beam) {

        float dx = beam.getEnd ().x - beam.getStart ().x;
        float dy = beam.getEnd ().y - beam.getStart ().y;
        float A = dx * dx + dy * dy;
        float B = 2 * ( dx * ( beam.getStart ().x - getCenter ().x ) + dy * ( beam.getStart ().y - getCenter ().y ) );
        float C = ( beam.getStart ().x - getCenter ().x ) * ( beam.getStart ().x - getCenter ().x ) +
                ( beam.getStart ().y - getCenter ().y ) * ( beam.getStart ().y - getCenter ().y ) -
                getRadius () * getRadius ();

        float det = B * B - 4 * A * C;

        if (A <= 0.01f || det < 0) return Vector2.Zero;
        if (det == 0.0f) {

            float t = -B / (2 * A);
            return tmp1.set (
                    beam.getStart ().x + t * dx,
                    beam.getStart ().y + t * dy
            );

        }

        float t1 = (float) ( -B + Math.sqrt ( det ) ) / ( 2f * A );
        float t2 = (float) ( -B - Math.sqrt ( det ) ) / ( 2f * A );

        tmp1.set (
                beam.getStart ().x + t1 * dx,
                beam.getStart ().y + t1 * dy
        );

        tmp2.set (
                beam.getStart ().x + t2 * dx,
                beam.getStart ().y + t2 * dy
        );

        if (tmp1.dst (beam.getStart ()) < tmp2.dst (beam.getStart ())) return tmp1;
        return tmp2;
    }

    @Override
    public void reset () {

        center.set (0.0f, 0.0f);
        radius = 0.0f;

    }
}
