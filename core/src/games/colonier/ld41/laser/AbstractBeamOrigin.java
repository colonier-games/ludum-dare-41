package games.colonier.ld41.laser;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBeamOrigin implements BeamOrigin {

    private List<Beam> originatingBeams = new ArrayList<> ();

    @Override
    public List<Beam> getOriginatingBeams () {
        return originatingBeams;
    }

    public void addHittingBeam (Beam b) {
        originatingBeams.add (b);
    }

    public void clearHittingBeams () {
        originatingBeams.clear ();
    }

}
