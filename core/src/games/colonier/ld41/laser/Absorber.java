package games.colonier.ld41.laser;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

import java.io.Serializable;

public abstract class Absorber extends AbstractBeamHittable implements Serializable, Pool.Poolable {

    public abstract boolean intersectsWithBeam (Beam beam);
    public abstract Vector2 getBeamCutPoint (Beam beam);

}
