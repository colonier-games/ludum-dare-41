package games.colonier.ld41.laser;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

import java.io.Serializable;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class Laser extends AbstractBeamOrigin implements Serializable, Pool.Poolable {

    private static AtomicInteger idSequence = new AtomicInteger (0);

    private int id = idSequence.incrementAndGet ();

    private Vector2 position;
    private Vector2 direction;
    private float length;

    public boolean active = true;

    public Laser (Vector2 position, Vector2 direction, float length) {
        this.position = position;
        this.direction = direction;
        this.length = length;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (!( o instanceof Laser )) return false;
        Laser laser = (Laser) o;
        return id == laser.id;
    }

    @Override
    public int hashCode () {

        return Objects.hash (id);
    }

    @Override
    public String toString () {
        return "Laser{" +
                "id=" + id +
                ", position=" + position +
                ", direction=" + direction +
                ", length=" + length +
                '}';
    }

    @Override
    public void reset () {

        position.set (0.0f, 0.0f);
        direction.set (0.0f, 0.0f);
        length = 0.0f;
        id = idSequence.incrementAndGet ();

    }

    public Vector2 getPosition () {
        return position;
    }

    public void setPosition (Vector2 position) {
        this.position.set (position);
    }

    public Vector2 getDirection () {
        return direction;
    }

    public void setDirection (Vector2 direction) {
        this.direction.set (direction);
    }

    public float getLength () {
        return length;
    }

    public void setLength (float length) {
        this.length = length;
    }
}
