package games.colonier.ld41.laser;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBeamOriginAndHittable extends AbstractBeamHittable implements BeamOrigin {

    private List<Beam> originatingBeams = new ArrayList<> ();

    @Override
    public List<Beam> getOriginatingBeams () {
        return originatingBeams;
    }

    public void addOriginatingBeam (Beam b) {
        originatingBeams.add (b);
    }

    public void clearOriginatingBeams () {
        originatingBeams.clear ();
    }

}
