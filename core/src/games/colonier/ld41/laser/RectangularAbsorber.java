package games.colonier.ld41.laser;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;

public class RectangularAbsorber extends Absorber {

    private Vector2 center;
    private Vector2 halfDimensions;

    private boolean cornersCalculated = false;
    private Vector2 topLeft = new Vector2 ();
    private Vector2 topRight = new Vector2 ();
    private Vector2 bottomRight = new Vector2 ();
    private Vector2 bottomLeft = new Vector2 ();

    Vector2[] intersectionPoints = new Vector2[] {
            new Vector2 (),
            new Vector2 (),
            new Vector2 (),
            new Vector2 ()
    };

    boolean[] intersections = new boolean[4];

    public RectangularAbsorber (Vector2 center, Vector2 halfDimensions) {
        this.center = center;
        this.halfDimensions = halfDimensions;
    }

    public Vector2 getCenter () {
        return center;
    }

    public void setCenter (Vector2 center) {
        this.center = center;

        cornersCalculated = false;
    }

    public Vector2 getHalfDimensions () {
        return halfDimensions;
    }

    public void setHalfDimensions (Vector2 halfDimensions) {
        this.halfDimensions = halfDimensions;

        cornersCalculated = false;
    }

    private void calculateCorners () {

        if (!cornersCalculated) {

            topLeft.set (
                    center.x - halfDimensions.x,
                    center.y + halfDimensions.y
            );

            topRight.set (
                    center.x + halfDimensions.x,
                    center.y + halfDimensions.y
            );

            bottomRight.set (
                    center.x + halfDimensions.x,
                    center.y - halfDimensions.y
            );

            bottomLeft.set (
                    center.x - halfDimensions.x,
                    center.y - halfDimensions.y
            );

            cornersCalculated = true;
        }

    }

    public Vector2 getTopLeft () {
        calculateCorners ();
        return topLeft;
    }

    public Vector2 getTopRight () {
        calculateCorners ();
        return topRight;
    }

    public Vector2 getBottomRight () {
        calculateCorners ();
        return bottomRight;
    }

    public Vector2 getBottomLeft () {
        calculateCorners ();
        return bottomLeft;
    }

    @Override
    public boolean intersectsWithBeam (Beam beam) {

        intersections [0] = Intersector.intersectSegments (
                beam.getStart (), beam.getEnd (),
                getBottomLeft (), getTopLeft (),
                intersectionPoints[0]
        );

        intersections [1] = Intersector.intersectSegments (
                beam.getStart (), beam.getEnd (),
                getTopLeft (), getTopRight (),
                intersectionPoints[1]
        );

        intersections [2] = Intersector.intersectSegments (
                beam.getStart (), beam.getEnd (),
                getTopRight (), getBottomRight (),
                intersectionPoints[2]
        );

        intersections [3] = Intersector.intersectSegments (
                beam.getStart (), beam.getEnd (),
                getBottomRight (), getBottomLeft (),
                intersectionPoints[3]
        );

        return intersections[0]
                || intersections[1]
                || intersections[2]
                || intersections[3];

    }

    @Override
    public Vector2 getBeamCutPoint (Beam beam) {

        for (int i = 0; i < 4; i++) intersectionPoints [i].set (0f, 0f);

        intersections [0] = Intersector.intersectSegments (
                beam.getStart (), beam.getEnd (),
                getBottomLeft (), getTopLeft (),
                intersectionPoints[0]
        );

        intersections [1] = Intersector.intersectSegments (
                beam.getStart (), beam.getEnd (),
                getTopLeft (), getTopRight (),
                intersectionPoints[1]
        );

        intersections [2] = Intersector.intersectSegments (
                beam.getStart (), beam.getEnd (),
                getTopRight (), getBottomRight (),
                intersectionPoints[2]
        );

        intersections [3] = Intersector.intersectSegments (
                beam.getStart (), beam.getEnd (),
                getBottomRight (), getBottomLeft (),
                intersectionPoints[3]
        );

        Vector2 result = null;

        for (int i = 0; i < intersections.length; i++) {

            if (intersections[i]
                    && ( result == null
                    || intersectionPoints[i].dst (beam.getStart ()) < result.dst (beam.getStart ()) )
                    ) {

                result = intersectionPoints[i];

            }

        }

        return result;
    }

    @Override
    public void reset () {

        center.set (0.0f, 0.0f);
        halfDimensions.set (0.0f, 0.0f);
        cornersCalculated = false;

    }
}
