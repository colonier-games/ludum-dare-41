package games.colonier.ld41.laser;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBeamHittable implements BeamHittable {

    private List<Beam> hittingBeams = new ArrayList<> ();

    @Override
    public List<Beam> getHittingBeams () {
        return hittingBeams;
    }

    public void addHittingBeam (Beam b) {
        hittingBeams.add (b);
    }

    public void clearHittingBeams () {
        hittingBeams.clear ();
    }


}
