package games.colonier.ld41.laser.gfx;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import games.colonier.ld41.laser.Beam;

import java.util.Random;

public class BeamDrawer {

    private final Random random;

    public BeamDrawer (Random random) {
        this.random = random;
    }

    public void draw (
            Beam beam,
            ShapeRenderer shapeRenderer
    ) {

        shapeRenderer.rectLine (

                beam.getStart ().x,
                beam.getStart ().y,
                beam.getEnd ().x,
                beam.getEnd ().y,

                2f + random.nextFloat () * 2f

        );

    }

}
