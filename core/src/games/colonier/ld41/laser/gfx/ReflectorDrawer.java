package games.colonier.ld41.laser.gfx;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import games.colonier.ld41.laser.Reflector;

public class ReflectorDrawer {

    public void draw (
            Reflector reflector,
            ShapeRenderer shapeRenderer
    ) {

        if (reflector.active) {
            shapeRenderer.setColor (Color.BLACK);

            shapeRenderer.rectLine (

                    reflector.getLineSegmentStart ().x,
                    reflector.getLineSegmentStart ().y,
                    reflector.getLineSegmentEnd ().x,
                    reflector.getLineSegmentEnd ().y,

                    5f

            );

            shapeRenderer.setColor (Color.WHITE);

            shapeRenderer.rectLine (

                    reflector.getLineSegmentStart ().x,
                    reflector.getLineSegmentStart ().y,
                    reflector.getLineSegmentEnd ().x,
                    reflector.getLineSegmentEnd ().y,

                    2f

            );
        }

    }

}
