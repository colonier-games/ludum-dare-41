package games.colonier.ld41.laser.gfx;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.constants.ObjectTextures;
import games.colonier.ld41.laser.Laser;

public class LaserDrawer {

    public void draw (
            Laser laser,
            SpriteBatch spriteBatch
    ) {

        spriteBatch.draw (
                ObjectTextures.LASER_REGION (),
                laser.getPosition ().x - Numbers.TILE_SCALE / 2f,
                laser.getPosition ().y - Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE / 2f,
                Numbers.TILE_SCALE,
                Numbers.TILE_SCALE,
                1f, 1f,
                laser.getDirection ()
                        .angle ()
        );

    }

}
