package games.colonier.ld41.laser;

import java.util.List;

public interface BeamOrigin {

    List<Beam> getOriginatingBeams ();

}
