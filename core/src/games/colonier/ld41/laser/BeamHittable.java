package games.colonier.ld41.laser;

import java.util.List;

public interface BeamHittable {

    List <Beam> getHittingBeams ();

}
