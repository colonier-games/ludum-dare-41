package games.colonier.ld41.laser;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Reflector extends AbstractBeamOriginAndHittable implements Serializable, Pool.Poolable, BeamHittable {

    private Vector2 position;
    private Vector2 normal;
    private float width = 10.0f;

    private Vector2 lineSegmentStart = new Vector2 ();
    private Vector2 lineSegmentEnd = new Vector2 ();

    private boolean lineSegmentCalculated = false;

    public boolean active = true;

    public Reflector (Vector2 position, Vector2 normal, float width) {
        this.position = position;
        this.normal = normal;
        this.width = width;
    }

    @Override
    public String toString () {
        return "Reflector{" +
                "position=" + position +
                ", normal=" + normal +
                ", width=" + width +
                '}';
    }

    public Vector2 getPosition () {
        return position;
    }

    public void setPosition (Vector2 position) {
        this.position = position;

        lineSegmentCalculated = false;
    }

    public Vector2 getNormal () {
        return normal;
    }

    public void setNormal (Vector2 normal) {
        this.normal = normal;

        lineSegmentCalculated = false;
    }

    public float getWidth () {
        return width;
    }

    public void setWidth (float width) {
        this.width = width;

        lineSegmentCalculated = false;
    }

    public Vector2 getLineSegmentStart () {

        calculateLineSegment ();

        return lineSegmentStart;
    }

    public Vector2 getLineSegmentEnd () {

        calculateLineSegment ();

        return lineSegmentEnd;
    }

    private void calculateLineSegment () {
        if (!lineSegmentCalculated) {

            Vector2 normalRot90_plus_scl = normal.cpy ().rotate90 (1).scl (width);
            Vector2 normalRot90_minus_scl = normal.cpy ().rotate90 (-1).scl (width);

            lineSegmentStart.set (
                    position.x + normalRot90_plus_scl.x,
                    position.y + normalRot90_plus_scl.y
            );

            lineSegmentEnd.set (
                    position.x + normalRot90_minus_scl.x,
                    position.y + normalRot90_minus_scl.y
            );

            lineSegmentCalculated = true;
        }
    }

    @Override
    public void reset () {

        position.set (0.0f, 0.0f);
        normal.set (0.0f, 0.0f);
        width = 0.0f;
        lineSegmentCalculated = false;

    }

}
