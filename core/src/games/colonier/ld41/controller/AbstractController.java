package games.colonier.ld41.controller;

import games.colonier.ld41.state.GameState;
import games.colonier.ld41.state.spec.PlayState;

public abstract class AbstractController <StateType extends GameState>
        implements GameController <StateType> {

    protected StateType gameState;

    public AbstractController (StateType gameState) {
        this.gameState = gameState;
    }

}
