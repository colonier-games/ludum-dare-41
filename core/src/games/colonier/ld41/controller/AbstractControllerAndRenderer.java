package games.colonier.ld41.controller;

import games.colonier.ld41.state.GameState;
import games.colonier.ld41.state.spec.PlayState;

public abstract class AbstractControllerAndRenderer <StateType extends GameState>
        extends AbstractController <StateType> implements GameRenderer <StateType> {

    public AbstractControllerAndRenderer (StateType gameState) {
        super (gameState);
    }

}
