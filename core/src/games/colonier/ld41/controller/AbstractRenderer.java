package games.colonier.ld41.controller;

import games.colonier.ld41.state.GameState;

public abstract class AbstractRenderer <StateType extends GameState>
        implements GameRenderer <StateType> {

    protected StateType gameState;

    public AbstractRenderer (StateType gameState) {
        this.gameState = gameState;
    }

}
