package games.colonier.ld41.controller;

import games.colonier.ld41.game.World;
import games.colonier.ld41.state.GameState;

public interface GameController <StateType extends GameState> {

    String getControllerName ();
    void init ();
    void update (float delta, StateType state);

}
