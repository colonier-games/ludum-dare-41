package games.colonier.ld41.controller.impl;

import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.annotation.Controller;
import games.colonier.ld41.config.DifficultyConfig;
import games.colonier.ld41.config.ScoresConfig;
import games.colonier.ld41.constants.Messages;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.constants.Sounds;
import games.colonier.ld41.controller.AbstractController;
import games.colonier.ld41.entity.Enemy;
import games.colonier.ld41.entity.GameEntity;
import games.colonier.ld41.entity.Player;
import games.colonier.ld41.game.World;
import games.colonier.ld41.state.impl.GameOverStateImpl;
import games.colonier.ld41.state.spec.PlayState;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Controller (order = 0)
public class PlayerController extends AbstractController <PlayState> {

    Random random = new Random ();

    public PlayerController (PlayState playState) {
        super (playState);
    }

    @Override
    public String getControllerName () {
        return "PlayerController";
    }

    @Override
    public void init () {

    }

    @Override
    public void update (float delta, PlayState playState) {

        World world = playState.getWorld ();

        world.players.stream ()
                .forEach (
                        player -> {

                            if (DifficultyConfig.getInstance ()
                                    .getDifficulty ().mirrorIsAlwaysOn) {
                                player.playerReflector.active = true;
                            }

                            float cosDeg = MathUtils.cosDeg (player.rotation);
                            float sinDeg = MathUtils.sinDeg (player.rotation);

                            float speed = player.playerReflector.active
                                    ? DifficultyConfig.getInstance ()
                                    .getDifficulty ()
                                    .playerSpeedWithReflector
                                    : DifficultyConfig
                                    .getInstance ()
                                    .getDifficulty ()
                                    .playerSpeedWithoutReflector;

                            player.position.add (
                                    ( player.movement.moveRight ? 1f : player.movement.moveLeft ? -1f : 0f )
                                            * ( ( player.movement.moveRight || player.movement.moveLeft ) && ( player.movement.moveUp || player.movement.moveDown ) ? 0.707f : 1f )
                                            * speed * Numbers.TILE_SCALE * delta,
                                    ( player.movement.moveUp ? 1f : player.movement.moveDown ? -1f : 0f )
                                            * ( ( player.movement.moveRight || player.movement.moveLeft ) && ( player.movement.moveUp || player.movement.moveDown ) ? 0.707f : 1f )
                                            * speed * Numbers.TILE_SCALE * delta
                            );

                            player.playerReflector.setNormal (
                                    new Vector2 (
                                            cosDeg,
                                            sinDeg
                                    ).nor ()
                            );

                            player.playerReflector.setPosition (
                                    player.position.cpy ()
                                            .add (
                                                    player.playerReflector.getNormal ()
                                                            .cpy ()
                                                            .scl (Numbers.TILE_SCALE)
                                            )
                            );

                        }
                );

        world.entities.stream ()
                .forEach (
                        entity -> {

                            entity.getAbsorber ()
                                    .setCenter (entity.getPosition ());

                        }
                );

        List <GameEntity> entitiesToRemove = new ArrayList <> ();

        world.entities.stream ()
                .forEach (

                        entity -> {

                            if (!entity.getAbsorber ()
                                    .getHittingBeams ()
                                    .isEmpty ()) {

                                entity.setHealth (
                                        (float) ( entity.getHealth ()
                                                - entity.getAbsorber ()
                                                .getHittingBeams ()
                                                .stream ()
                                                .mapToDouble (b -> DifficultyConfig.getInstance ()
                                                        .getDifficulty ().beamDamage)
                                                .sum () * delta )
                                );

                                if (entity.getHealth () <= 0f) {

                                    // killAndRespawnPlayer (entity, world);
                                    entitiesToRemove.add (entity);

                                }

                            }

                        }

                );

        world.entities.removeAll (entitiesToRemove);

        entitiesToRemove.stream ()
                .filter (e -> e instanceof Enemy)
                .map (e -> (Enemy) e)
                .forEach (
                        enemy -> {

                            MessageManager.getInstance ()
                                    .removeListener (
                                            enemy.getStateMachine (),
                                            Messages.PLAYER_MOVED
                                    );

                        }
                );

        world.players.removeAll (
                entitiesToRemove.stream ()
                        .filter (e -> e instanceof Player)
                        .collect (Collectors.toList ())
        );

        if (!entitiesToRemove.isEmpty ()) {

            Sounds.EXPLOSION ()
                    .play ();

            ScoresConfig.getInstance ().currentScore += entitiesToRemove.stream ()
                    .filter (e -> !( e instanceof Player ))
                    .count ();

        }

        if (world.players.isEmpty ()) {

            gameState.changeState (GameOverStateImpl.class);

        }

    }

}
