package games.colonier.ld41.controller.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import games.colonier.ld41.annotation.Controller;
import games.colonier.ld41.annotation.Renderer;
import games.colonier.ld41.config.DifficultyConfig;
import games.colonier.ld41.config.EntityConfig;
import games.colonier.ld41.constants.Fonts;
import games.colonier.ld41.constants.Sounds;
import games.colonier.ld41.controller.AbstractController;
import games.colonier.ld41.controller.AbstractControllerAndRenderer;
import games.colonier.ld41.entity.Enemy;
import games.colonier.ld41.entity.enemy.EnemyType;
import games.colonier.ld41.entity.enemy.circler.CirclerEnemy;
import games.colonier.ld41.entity.enemy.simple.SimpleEnemy;
import games.colonier.ld41.state.spec.PlayState;

import java.util.Random;

@Controller (order = 20)
@Renderer (order = 20)
public class WaveController extends AbstractControllerAndRenderer <PlayState> {

    public float timeBetweenWaves = 5f;
    public float waitTimer = 0f;
    public int numEnemiesToSpawn = 10;
    public int numSpawnedEnemies = 0;
    public float spawnIncrementFactor = 1.2f;
    public float spawnTimer = 0f;
    public float timeBetweenSpawns = 2f;
    public WaveState waveState = WaveState.WAIT;

    int lastCountdown = -1;

    Random random = new Random (System.currentTimeMillis ());

    private SpriteBatch spriteBatch;

    GlyphLayout glyphLayout = new GlyphLayout ();

    public WaveController (PlayState gameState) {
        super (gameState);
    }

    @Override
    public String getControllerName () {
        return "WaveController";
    }

    @Override
    public String getRendererName () {
        return "WaveRenderer";
    }

    @Override
    public void init () {

        spriteBatch = new SpriteBatch ();

        numEnemiesToSpawn = DifficultyConfig.getInstance ()
                .getDifficulty ().initialNumberOfEnemies;
        spawnIncrementFactor = DifficultyConfig.getInstance ()
                .getDifficulty ().spawnIncrementFactor;
        timeBetweenSpawns = DifficultyConfig.getInstance ()
                .getDifficulty ().timeBetweenSpawns;

    }

    @Override
    public void render (float delta, PlayState state) {

        if (waveState == WaveState.WAIT) {

            if (timeBetweenWaves - waitTimer <= 3) {

                int countdown = (int) ( (timeBetweenWaves - waitTimer) + 1 );

                if (countdown != lastCountdown) {

                    Sounds.SELECT ().play ();

                }

                lastCountdown = countdown;

                spriteBatch.setProjectionMatrix (state.getGuiCamera ().combined);

                spriteBatch.begin ();

                glyphLayout.setText (
                        Fonts.bigBold (),
                        countdown + ""
                );

                Fonts.bigBold ()
                        .setColor (Color.BLACK);

                Fonts.bigBold ()
                        .draw (
                                spriteBatch,
                                countdown + "",
                                -glyphLayout.width / 2f,
                                Gdx.graphics.getHeight () / 4
                        );

                spriteBatch.end ();

            }

        }

    }

    @Override
    public void update (float delta, PlayState state) {

        long currentCount = state
                .getWorld ()
                .entities
                .stream ()
                .filter (
                        entity -> entity instanceof Enemy
                )
                .count ();

        switch (waveState) {

            case WAIT:

                waitTimer += delta;

                if (waitTimer >= timeBetweenWaves) {

                    waveState = WaveState.IN_PROGRESS;
                    waitTimer = 0f;

                    spawnTimer = 0f;
                    numSpawnedEnemies = 0;
                    numEnemiesToSpawn = (int) ( numEnemiesToSpawn * spawnIncrementFactor );

                }

                break;
            case IN_PROGRESS:

                if (numSpawnedEnemies == numEnemiesToSpawn
                        && currentCount == 0l) {

                    waveState = WaveState.WAIT;
                    waitTimer = 0f;

                } else if (numSpawnedEnemies < numEnemiesToSpawn) {

                    spawnTimer += delta;

                    while (spawnTimer >= timeBetweenSpawns) {

                        spawnTimer -= timeBetweenSpawns;

                        state
                                .getWorld ()
                                .entities
                                .add (
                                        getRandomEnemy (state)
                                );

                        ++numSpawnedEnemies;

                        System.out.println (
                                "Spawn " + numSpawnedEnemies + " / " + numEnemiesToSpawn
                        );

                        Sounds.SPAWN ().play ();

                    }

                }

                break;
        }

    }

    Enemy getRandomEnemy (PlayState state) {

        if (random.nextInt (10) < 4) {

            return new CirclerEnemy (
                    EntityConfig.getInstance ()
                    .newId (),
                    state,
                    state
                            .getWorld ()
                            .gameMap
                            .enemySpawnPoints
                            .get (
                                    random.nextInt (
                                            state.getWorld ()
                                                    .gameMap
                                                    .enemySpawnPoints
                                                    .size () - 1
                                    )
                            )
            );

        } else {

            return new SimpleEnemy (
                    EntityConfig.getInstance ()
                            .newId (),
                    state,
                    state
                            .getWorld ()
                            .gameMap
                            .enemySpawnPoints
                            .get (
                                    random.nextInt (
                                            state.getWorld ()
                                                    .gameMap
                                                    .enemySpawnPoints
                                                    .size () - 1
                                    )
                            )
            );

        }

    }

    public enum WaveState {

        WAIT,
        IN_PROGRESS

    }

}
