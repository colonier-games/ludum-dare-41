package games.colonier.ld41.controller.impl;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import games.colonier.ld41.annotation.Controller;
import games.colonier.ld41.annotation.Renderer;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.controller.AbstractControllerAndRenderer;
import games.colonier.ld41.game.World;
import games.colonier.ld41.laser.RectangularAbsorber;
import games.colonier.ld41.state.spec.PlayState;

@Controller (order = 100)
@Renderer (order = 100)
public class DebugController extends AbstractControllerAndRenderer<PlayState> {

    private static final boolean DEBUG = false;

    private ShapeRenderer shapeRenderer;

    public DebugController (PlayState playState) {
        super (playState);
    }

    @Override
    public String getControllerName () {
        return "DebugController";
    }

    @Override
    public String getRendererName () {
        return "DebugRenderer";
    }

    @Override
    public void init () {

        shapeRenderer = new ShapeRenderer ();

    }

    @Override
    public void render (
            float delta, PlayState playState
    ) {

        World world = playState.getWorld ();
        OrthographicCamera gameCamera = playState.getGameCamera ();
        OrthographicCamera guiCamera = playState.getGuiCamera ();

        if (DEBUG) {

            shapeRenderer.setProjectionMatrix (gameCamera.combined);

            shapeRenderer.begin (ShapeRenderer.ShapeType.Line);

            shapeRenderer.setColor (Color.WHITE);

            ( (GameLogicController) gameState.getController ("GameLogicController") ).allAbsorbers.stream ()
                    .forEach (
                            absorber -> {

                                if (absorber instanceof RectangularAbsorber) {

                                    RectangularAbsorber rect = (RectangularAbsorber) absorber;

                                    shapeRenderer.polygon (
                                            new float[] {
                                                    rect.getTopLeft ().x,
                                                    rect.getTopLeft ().y,
                                                    rect.getTopRight ().x,
                                                    rect.getTopRight ().y,
                                                    rect.getBottomRight ().x,
                                                    rect.getBottomRight ().y,
                                                    rect.getBottomLeft ().x,
                                                    rect.getBottomLeft ().y
                                            }
                                    );

                                }

                            }
                    );

            world.players
                    .stream ()
                    .forEach (
                            player -> {

                                int tileXFrom = (int) ( Math.floor (
                                        player.position.x / Numbers.TILE_SCALE
                                ) - 1 );

                                int tileYFrom = (int) ( Math.floor (
                                        player.position.y / Numbers.TILE_SCALE
                                ) - 1 );

                                int tileXTo = (int) ( Math.floor (
                                        player.position.x / Numbers.TILE_SCALE
                                ) + 1 );

                                int tileYTo = (int) ( Math.floor (
                                        player.position.y / Numbers.TILE_SCALE
                                ) + 1 );

                                for (int y = tileYFrom; y <= tileYTo; y++) {

                                    for (int x = tileXFrom; x <= tileXTo; x++) {

                                        shapeRenderer.setColor (Color.BLUE);

                                        if (x >= 0 && x < world.gameMap.tileCollidability [0].length
                                                && y >= 0 && y < world.gameMap.tileCollidability.length
                                                && !world.gameMap.tileCollidability [y][x]) {

                                            shapeRenderer.setColor (Color.RED);

                                        }

                                        shapeRenderer.rect (
                                                x * Numbers.TILE_SCALE,
                                                y * Numbers.TILE_SCALE,
                                                Numbers.TILE_SCALE,
                                                Numbers.TILE_SCALE
                                        );

                                    }

                                }

                            }
                    );

            shapeRenderer.end ();

        }


    }

    @Override
    public void update (float delta, PlayState playState) {

        World world = playState.getWorld ();

        if (DEBUG) {


        }

    }
}
