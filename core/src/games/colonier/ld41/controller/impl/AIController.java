package games.colonier.ld41.controller.impl;

import com.badlogic.gdx.ai.GdxAI;
import com.badlogic.gdx.ai.Timepiece;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.annotation.Controller;
import games.colonier.ld41.constants.Messages;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.controller.AbstractController;
import games.colonier.ld41.entity.Enemy;
import games.colonier.ld41.entity.Player;
import games.colonier.ld41.entity.enemy.simple.SimpleEnemyState;
import games.colonier.ld41.map.TileCoords;
import games.colonier.ld41.state.spec.PlayState;

import java.util.HashMap;
import java.util.Map;

@Controller (order = 2)
public class AIController extends AbstractController <PlayState> {

    private Map <Player, TileCoords> lastPlayerTileCoords = new HashMap <> ();

    TileCoords playerTileCoords = new TileCoords (0, 0);
    Vector2 playerPosition = new Vector2 ();
    Vector2 tmp = new Vector2 ();
    Vector2 tmp2 = new Vector2 ();

    public AIController (PlayState gameState) {
        super (gameState);
    }

    @Override
    public String getControllerName () {
        return "AIController";
    }

    @Override
    public void init () {

    }

    @Override
    public void update (float delta, PlayState state) {

        state.getWorld ()
                .players
                .stream ()
                .forEach (
                        player -> {

                            TileCoords playerCoords = new TileCoords (
                                    (int) (player.position.x / Numbers.TILE_SCALE),
                                    (int) (player.position.y / Numbers.TILE_SCALE)
                            );

                            TileCoords lastPlayerCoords = lastPlayerTileCoords.get (player);

                            if (lastPlayerCoords == null
                                    || !lastPlayerCoords.equals (playerCoords)) {

                                MessageManager.getInstance ()
                                        .dispatchMessage (
                                                Messages.PLAYER_MOVED
                                        );

                            }

                            lastPlayerTileCoords.put (
                                    player,
                                    playerCoords
                            );

                        }
                );

        playerPosition.set (
                state.getGamePlayers ()
                        .get (0).position
        );

        playerTileCoords.set (
                (int) ( playerPosition.x / Numbers.TILE_SCALE ),
                (int) ( playerPosition.y / Numbers.TILE_SCALE )
        );


        state.getWorld ()
                .entities
                .stream ()
                .filter (e -> e instanceof Enemy)
                .map (e -> (Enemy) e)
                .forEach (
                        enemy -> {

                            StateMachine stateMachine = enemy.getStateMachine ();

                            if (stateMachine == null) {

                                enemy.setStateMachine (
                                        enemy.createStateMachine ()
                                );

                                stateMachine = enemy.getStateMachine ();

                                MessageManager.getInstance ()
                                        .addListener (stateMachine, Messages.PLAYER_MOVED);

                            }

                            stateMachine.update ();

                        }
                );

    }
}
