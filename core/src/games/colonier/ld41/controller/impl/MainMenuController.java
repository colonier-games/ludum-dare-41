package games.colonier.ld41.controller.impl;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.annotation.Controller;
import games.colonier.ld41.annotation.Renderer;
import games.colonier.ld41.constants.Fonts;
import games.colonier.ld41.constants.Strings;
import games.colonier.ld41.controller.AbstractControllerAndRenderer;
import games.colonier.ld41.menu.MenuItem;
import games.colonier.ld41.state.GameState;
import games.colonier.ld41.state.spec.MainMenuState;

@Controller (
        order = 1,
        gameStates = Strings.GAME_STATE_MAIN_MENU
)
@Renderer (
        order = 1,
        gameStates = Strings.GAME_STATE_MAIN_MENU
)
public class MainMenuController extends AbstractControllerAndRenderer <MainMenuState> {

    SpriteBatch spriteBatch;
    GlyphLayout glyphLayout = new GlyphLayout ();

    public float lerpTimer = 0f;
    final float totalLerpTime = 1f;
    public Vector2 lerpTowards = new Vector2 ();
    int lastMenuItemIndex = 0;

    public MainMenuController (MainMenuState gameState) {
        super (gameState);
    }

    @Override
    public String getControllerName () {
        return "MainMenuController";
    }

    @Override
    public String getRendererName () {
        return "MainMenuRenderer";
    }

    @Override
    public void init () {

        spriteBatch = new SpriteBatch ();

    }

    @Override
    public void render (float delta, MainMenuState state) {

        spriteBatch.setProjectionMatrix (state.getGuiCamera ().combined);

        spriteBatch.begin ();

        int offset = 0;

        int currentMenuItemIndex = 0;
        for (int i = 0; i < state.getCurrentMenu ().getMenuItems ().size (); i++) {
            if (state.getCurrentMenu ().getMenuItems ().get (i).equals (state.getActiveMenuItem ())) {
                currentMenuItemIndex = i;
                break;
            }
        }

        // Draw current menu item
        glyphLayout.setText (
                Fonts.bigBold (),
                state.getActiveMenuItem ().getText ()
        );

        Fonts.bigBold ()
                .setColor (Color.BLUE);

        Fonts.bigBold ()
                .draw (
                        spriteBatch,
                        state.getActiveMenuItem ().getText (),
                        -glyphLayout.width / 2f,
                        0
                );

        Fonts.small ()
                .setColor (Color.GRAY);

        offset = 1;

        // Draw menu items before current
        for (int i = currentMenuItemIndex - 1; i >= 0; i--) {

            MenuItem menuItem = state.getCurrentMenu ().getMenuItems ().get (i);

            if (!menuItem.getIsRenderedPredicate ().test (menuItem)) continue;

            glyphLayout.setText (
                    Fonts.small (),
                    menuItem.getText ()
            );

            Fonts.small ()
                    .draw (
                            spriteBatch,
                            menuItem.getText (),
                            -glyphLayout.width / 2f + lerpTowards.x,
                            40f * (offset++) + lerpTowards.y
                    );

        }

        offset = 1;

        // Draw menu items after current
        for (int i = currentMenuItemIndex + 1; i < state.getCurrentMenu ().getMenuItems ().size (); i++) {

            MenuItem menuItem = state.getCurrentMenu ().getMenuItems ().get (i);

            if (!menuItem.getIsRenderedPredicate ().test (menuItem)) continue;

            glyphLayout.setText (
                    Fonts.small (),
                    menuItem.getText ()
            );

            Fonts.small ()
                    .draw (
                            spriteBatch,
                            menuItem.getText (),
                            -glyphLayout.width / 2f + lerpTowards.x,
                             - (40f * (offset++)) + lerpTowards.y
                    );

        }

        spriteBatch.end ();

    }

    @Override
    public void update (float delta, MainMenuState state) {

    }
}
