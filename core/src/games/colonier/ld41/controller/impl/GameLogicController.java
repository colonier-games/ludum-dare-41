package games.colonier.ld41.controller.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import games.colonier.ld41.annotation.Controller;
import games.colonier.ld41.annotation.Renderer;
import games.colonier.ld41.constants.Colors;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.constants.ObjectTextures;
import games.colonier.ld41.controller.AbstractControllerAndRenderer;
import games.colonier.ld41.entity.Enemy;
import games.colonier.ld41.entity.GameEntity;
import games.colonier.ld41.entity.gfx.EnemyDrawer;
import games.colonier.ld41.entity.gfx.PlayerDrawer;
import games.colonier.ld41.game.World;
import games.colonier.ld41.gfx.BeamParticle;
import games.colonier.ld41.laser.*;
import games.colonier.ld41.laser.gfx.BeamDrawer;
import games.colonier.ld41.laser.gfx.LaserDrawer;
import games.colonier.ld41.laser.gfx.ReflectorDrawer;
import games.colonier.ld41.state.spec.PlayState;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Controller (order = 10)
@Renderer (order = 10)
public class GameLogicController extends AbstractControllerAndRenderer <PlayState> {

    public List <Laser> allLasers = new ArrayList <> ();
    public List <Reflector> allReflectors = new ArrayList <> ();
    public List <Absorber> allAbsorbers = new ArrayList <> ();
    public List <Beam> beams = new ArrayList <> ();
    private SpriteBatch spriteBatch;
    private ShapeRenderer shapeRenderer;
    private LaserSimulation laserSimulation;

    Pool <BeamParticle> beamParticlePool = new Pool <BeamParticle> () {
        @Override
        protected BeamParticle newObject () {
            return new BeamParticle ();
        }
    };

    List <BeamParticle> aliveBeamParticles = new ArrayList <> ();
    List <BeamParticle> beamParticlesToRemove = new ArrayList <> ();

    private BeamDrawer beamDrawer;
    private ReflectorDrawer reflectorDrawer;
    private LaserDrawer laserDrawer;
    private PlayerDrawer playerDrawer;
    private EnemyDrawer enemyDrawer;

    public GameLogicController (PlayState playState) {
        super (playState);
    }

    @Override
    public String getControllerName () {
        return "GameLogicController";
    }

    @Override
    public String getRendererName () {
        return "GameLogicRenderer";
    }

    @Override
    public void init () {

        laserSimulation = new LaserSimulation ();

        spriteBatch = new SpriteBatch ();
        shapeRenderer = new ShapeRenderer ();

        beamDrawer = new BeamDrawer (random);
        reflectorDrawer = new ReflectorDrawer ();
        laserDrawer = new LaserDrawer ();
        playerDrawer = new PlayerDrawer ();
        enemyDrawer = new EnemyDrawer ();

    }

    Random random = new Random (System.currentTimeMillis ());

    Vector2 tmp = new Vector2 ();

    @Override
    public void render (
            float delta, PlayState playState
    ) {


        World world = playState.getWorld ();
        OrthographicCamera gameCamera = playState.getGameCamera ();
        OrthographicCamera guiCamera = playState.getGuiCamera ();

        shapeRenderer.setProjectionMatrix (gameCamera.combined);

        shapeRenderer.begin (ShapeRenderer.ShapeType.Filled);

        shapeRenderer.setColor (Color.RED);

        beams.stream ()
                .forEach (
                        beam -> {

                            beamDrawer.draw (beam, shapeRenderer);

                        }
                );


        aliveBeamParticles
                .stream ()
                .forEach (
                        beamParticle -> {

                            shapeRenderer.rect (
                                    beamParticle.position.x - 1f,
                                    beamParticle.position.y - 1f,
                                    2f,
                                    2f
                            );

                        }
                );

        allReflectors.stream ()
                .forEach (
                        reflector -> {

                            reflectorDrawer.draw (reflector, shapeRenderer);

                        }
                );

        shapeRenderer.end ();

        spriteBatch.setProjectionMatrix (gameCamera.combined);

        spriteBatch.begin ();

        world.gameMap.mapLasers.stream ()
                .forEach (
                        laser -> {

                            laserDrawer.draw (laser, spriteBatch);

                        }
                );

        world.entities
                .stream ()
                .filter (
                        e -> e instanceof Enemy
                )
                .map (e -> (Enemy) e)
                .forEach (
                        enemy -> {

                            enemyDrawer.draw (enemy, spriteBatch);

                        }
                );

        world.players.stream ()
                .forEach (
                        player -> {

                            playerDrawer.draw (player, spriteBatch);

                        }
                );

        spriteBatch.end ();


        shapeRenderer.setProjectionMatrix (playState.getGuiCamera ().combined);

        shapeRenderer.begin (ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor (Color.BLACK);
        shapeRenderer.rect (
                -Gdx.graphics.getWidth () / 4f - 4f,
                Gdx.graphics.getHeight () / 2f - 40f,
                Gdx.graphics.getWidth () / 2f + 8f,
                28f
        );
        shapeRenderer.setColor (Color.RED);
        shapeRenderer.rect (
                -Gdx.graphics.getWidth () / 4f,
                Gdx.graphics.getHeight () / 2f - 36f,
                (Gdx.graphics.getWidth () / 2f) * (world.players.get (0).health / world.players.get (0).maxHealth),
                20f
        );

        shapeRenderer.end ();

    }

    @Override
    public void update (float delta, PlayState playState) {

        beamParticlesToRemove.clear ();

        World world = playState.getWorld ();

        allLasers.clear ();
        allReflectors.clear ();
        allAbsorbers.clear ();

        allLasers.addAll (world.gameMap.mapLasers.stream ().filter (l -> l.active).collect(Collectors.toList()));
        allReflectors.addAll (world.gameMap.mapReflectors);
        allAbsorbers.addAll (world.gameMap.mapAbsorbers);

        allReflectors.addAll (
                world.players.stream ()
                        .map (player -> player.playerReflector)
                        .filter (r -> r.active)
                        .collect (Collectors.toList ())
        );

        allAbsorbers.addAll (
                world.entities.stream ()
                        .map (GameEntity::getAbsorber)
                        .collect (Collectors.toList ())
        );

        allLasers.addAll (
                world.entities.stream ()
                        .filter (e -> e instanceof Enemy)
                        .map (Enemy.class::cast)
                        .map (e -> e.laser)
                        .filter (l -> l.active)
                        .collect (Collectors.toList ())
        );

        beams.clear ();

        beams.addAll (
                laserSimulation.simulate (
                        allLasers,
                        allReflectors,
                        allAbsorbers
                )
        );

        beams.stream ()
                .forEach (
                        beam -> {

                            if (random.nextBoolean ()) {

                                BeamParticle newParticle = beamParticlePool.obtain ();

                                float t = random.nextFloat ();

                                newParticle.initialPosition.set (
                                        beam.getStart ().x + t * (beam.getEnd ().x - beam.getStart ().x),
                                        beam.getStart ().y + t * (beam.getEnd ().y - beam.getStart ().y)
                                );

                                boolean b = random.nextBoolean ();

                                tmp.set (beam.getEnd ())
                                        .sub (beam.getStart ())
                                        .nor ();

                                newParticle.direction
                                        .set (
                                                b ? tmp.rotate90 (1) : tmp.rotate90 (-1)
                                        );

                                aliveBeamParticles.add (newParticle);

                            }

                        }
                );

        aliveBeamParticles.stream ()
                .forEach (bp -> bp.update (delta));

        beamParticlesToRemove.addAll (
                aliveBeamParticles.stream ()
                .filter (bp -> !bp.alive ())
                .collect(Collectors.toList())
        );

        beamParticlesToRemove.forEach (beamParticlePool::free);
        aliveBeamParticles.removeAll (beamParticlesToRemove);

    }

}
