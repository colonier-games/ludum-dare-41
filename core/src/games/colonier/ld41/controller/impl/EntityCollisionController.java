package games.colonier.ld41.controller.impl;

import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.annotation.Controller;
import games.colonier.ld41.constants.Messages;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.controller.AbstractController;
import games.colonier.ld41.entity.GameEntity;
import games.colonier.ld41.entity.Player;
import games.colonier.ld41.state.spec.PlayState;
import games.colonier.ld41.game.World;

import java.util.HashMap;
import java.util.Map;

@Controller (order = 50)
public class EntityCollisionController extends AbstractController <PlayState> {

    private static final float EPSILON = 1f / 16f;
    
    Map <GameEntity, Vector2> lastPositionsByEntity = new HashMap <> ();

    boolean [][] localTileCollidability = new boolean[][] {
            { false, false, false },
            { false, false, false },
            { false, false, false }
    };

    Vector2 velocity = new Vector2 ();

    Circle playerCircle = new Circle ();
    Rectangle blockRect = new Rectangle ();

    public EntityCollisionController (PlayState playState) {
        super (playState);
    }

    @Override
    public String getControllerName () {
        return "EntityCollisionController";
    }

    @Override
    public void init () {



    }

    @Override
    public void update (float delta, PlayState playState) {

        World world = playState.getWorld ();

        world.entities.stream ().forEach (
                entity -> {

                    if (!lastPositionsByEntity.containsKey (entity)) {
                        lastPositionsByEntity.put (
                                entity,
                                entity.getPosition ().cpy ()
                        );
                    } else {

                        Vector2 lastPosition = lastPositionsByEntity.get (entity);
                        Vector2 currentPosition = entity.getPosition ();

                        playerCircle.set (
                                currentPosition.x,
                                currentPosition.y,
                                Numbers.TILE_SCALE / 2f - 2f
                        );

                        for (int y = 0; y < world.gameMap.tileCollidability.length; y++) {

                            for (int x = 0; x < world.gameMap.tileCollidability [y].length; x++) {

                                if (!world.gameMap.tileCollidability [y][x]) {

                                    blockRect.set (
                                            x * Numbers.TILE_SCALE,
                                            y * Numbers.TILE_SCALE,
                                            Numbers.TILE_SCALE,
                                            Numbers.TILE_SCALE
                                    );

                                    // No collision, nothing to handle
                                    if (!Intersector.overlaps (playerCircle, blockRect)) continue;

                                    // Try to adjust on X axis
                                    playerCircle.set (
                                            lastPosition.x + EPSILON * Math.signum (lastPosition.x - currentPosition.x),
                                            currentPosition.y,
                                            Numbers.TILE_SCALE / 2f - 2f
                                    );

                                    if (Intersector.overlaps (playerCircle, blockRect)) {

                                        // Try to adjust on Y axis

                                        playerCircle.set (
                                                currentPosition.x,
                                                lastPosition.y + EPSILON * Math.signum (lastPosition.y - currentPosition.y),
                                                Numbers.TILE_SCALE / 2f - 2f
                                        );

                                        if (Intersector.overlaps (playerCircle, blockRect)) {

                                            // Still overlaps, must reset both axes

                                            currentPosition.set (
                                                    lastPosition.x + EPSILON * Math.signum (lastPosition.x - currentPosition.x),
                                                    lastPosition.y + EPSILON * Math.signum (lastPosition.y - currentPosition.y)
                                            );

                                        } else {

                                            // Adjusting on Y axis helped, apply

                                            currentPosition.set (
                                                    currentPosition.x,
                                                    lastPosition.y + EPSILON * Math.signum (lastPosition.y - currentPosition.y)
                                            );

                                        }

                                    } else {

                                        // Adjusting on X axis helped, apply the solution

                                        currentPosition.set (
                                                lastPosition.x + EPSILON * Math.signum (lastPosition.x - currentPosition.x),
                                                currentPosition.y
                                        );

                                    }

                                }

                            }

                        }

                        lastPositionsByEntity.get (
                                entity
                        ).set (currentPosition);

                    }

                }
        );



    }
}
