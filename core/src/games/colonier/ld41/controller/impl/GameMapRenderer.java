package games.colonier.ld41.controller.impl;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import games.colonier.ld41.annotation.Renderer;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.constants.TileTextures;
import games.colonier.ld41.controller.AbstractRenderer;
import games.colonier.ld41.state.spec.PlayState;
import games.colonier.ld41.game.World;

@Renderer (order = 0)
public class GameMapRenderer extends AbstractRenderer <PlayState> {

    private SpriteBatch spriteBatch;

    private TiledMapRenderer tiledMapRenderer;

    public GameMapRenderer (PlayState playState) {
        super (playState);
    }

    @Override
    public String getRendererName () {
        return "GameMapRenderer";
    }

    @Override
    public void init () {

        spriteBatch = new SpriteBatch ();
        tiledMapRenderer = new OrthogonalTiledMapRenderer (
                gameState.getWorld ().gameMap.tiledMap,
                Numbers.TILE_SCALE / 16f,
                spriteBatch
        );

    }

    @Override
    public void render (
            float delta, PlayState playState
    ) {

        World world = playState.getWorld ();
        OrthographicCamera gameCamera = playState.getGameCamera ();
        OrthographicCamera guiCamera = playState.getGuiCamera ();

        // tiledMapRenderer.setView ((OrthographicCamera) gameCamera);

        spriteBatch.setProjectionMatrix (gameCamera.combined);
        spriteBatch.begin ();

        world.gameMap.tiledMap
                .getLayers ()
                .getByType (TiledMapTileLayer.class)
                .forEach (layer -> {
                    for (int y = 0; y < layer.getHeight (); y++) {
                        for (int x = 0; x < layer.getWidth (); x++) {

                            TiledMapTileLayer.Cell cell = layer.getCell (x, y);

                            if (cell != null) {
                                spriteBatch.draw (
                                        cell.getTile ()
                                                .getTextureRegion (),
                                        x * Numbers.TILE_SCALE,
                                        y * Numbers.TILE_SCALE,
                                        Numbers.TILE_SCALE,
                                        Numbers.TILE_SCALE
                                );
                            }

                        }
                    }
                });

        spriteBatch.end ();

    }
}
