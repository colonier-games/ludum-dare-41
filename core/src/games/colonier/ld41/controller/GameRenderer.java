package games.colonier.ld41.controller;

import com.badlogic.gdx.graphics.Camera;
import games.colonier.ld41.game.World;
import games.colonier.ld41.state.GameState;

public interface GameRenderer <StateType extends GameState> {

    String getRendererName ();
    void init ();
    void render (float delta, StateType state);

}
