package games.colonier.ld41.menu;

import com.badlogic.gdx.graphics.Color;
import games.colonier.ld41.state.GameState;
import games.colonier.ld41.state.spec.MainMenuState;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class MenuItem {

    private String id;
    private String text;
    private SelectionCallback selectionCallback;
    private Predicate <MenuItem> isRenderedPredicate = (mi) -> true;
    private Function <MainMenuState, Color> colorFunction = (gameState) -> gameState.getActiveMenuItem ().equals (this) ? Color.BLUE : Color.RED;

    public static MenuItem create (
            String id,
            String text,
            SelectionCallback selectionCallback
    ) {

        MenuItem item = new MenuItem ();

        item.setId (id);
        item.setText (text);
        item.setSelectionCallback (selectionCallback);

        return item;

    }

    public MenuItem rendered (Predicate <MenuItem> pred) {
        isRenderedPredicate = pred;
        return this;
    }

    public MenuItem color (Function <MainMenuState, Color> func) {
        colorFunction = func;
        return this;
    }

    public String getText () {
        return text;
    }

    public void setText (String text) {
        this.text = text;
    }

    public SelectionCallback getSelectionCallback () {
        return selectionCallback;
    }

    public void setSelectionCallback (SelectionCallback selectionCallback) {
        this.selectionCallback = selectionCallback;
    }

    @FunctionalInterface
    public static interface SelectionCallback {

        void onSelected (MenuItem menuItem);

    }

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    public Predicate <MenuItem> getIsRenderedPredicate () {
        return isRenderedPredicate;
    }

    public void setIsRenderedPredicate (Predicate <MenuItem> isRenderedPredicate) {
        this.isRenderedPredicate = isRenderedPredicate;
    }

    public Function <MainMenuState, Color> getColorFunction () {
        return colorFunction;
    }

    public void setColorFunction (
            Function <MainMenuState, Color> colorFunction
    ) {
        this.colorFunction = colorFunction;
    }
}
