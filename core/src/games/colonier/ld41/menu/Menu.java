package games.colonier.ld41.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Menu implements Serializable {

    private String id;

    private String title;

    private List<MenuItem> menuItems = new ArrayList<> ();
    private Menu parentMenu = null;
    private List <Menu> subMenus = new ArrayList <> ();

    public static Menu create (
            String id,
            String title,
            MenuItems menuItems,
            Menus subMenus
    ) {

        Menu menu = new Menu ();

        menu.setTitle (title);
        menu.setId (id);

        if (menuItems != null) {
            menu.menuItems.addAll (menuItems.list);
        }

        if (subMenus != null) {
            menu.subMenus.addAll (subMenus.list);
        }

        menu.subMenus.stream ().forEach (subMenu -> subMenu.setParentMenu (menu));

        return menu;

    }

    public List <MenuItem> getMenuItems () {
        return menuItems;
    }

    public void setMenuItems (List <MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public Menu getParentMenu () {
        return parentMenu;
    }

    public void setParentMenu (Menu parentMenu) {
        this.parentMenu = parentMenu;
    }

    public List <Menu> getSubMenus () {
        return subMenus;
    }

    public void setSubMenus (List <Menu> subMenus) {
        this.subMenus = subMenus;
    }

    public static class MenuItems implements Serializable {

        public List <MenuItem> list = new ArrayList <> ();

        public static MenuItems of (MenuItem ... args) {

            MenuItems result = new MenuItems ();

            for (MenuItem arg : args) result.list.add (arg);

            return result;

        }

        public static MenuItems of (List <MenuItem> list) {

            MenuItems result = new MenuItems ();

            for (MenuItem arg : list) result.list.add (arg);

            return result;

        }

    }

    public static class Menus implements Serializable {

        public List <Menu> list = new ArrayList <> ();

        public static Menus of (Menu ... args) {

            Menus result = new Menus ();

            for (Menu arg : args) result.list.add (arg);

            return result;

        }

        public static Menus of (List <Menu> list) {

            Menus result = new Menus ();

            for (Menu arg : list) result.list.add (arg);

            return result;

        }

    }

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    public Menu findMenu (String id) {

        if (getId ().equals (id)) return this;

        for (Menu child : subMenus) {

            Menu found = child.findMenu (id);

            if (found != null) return found;

        }

        return null;

    }

    public MenuItem findItem (String id) {

        for (MenuItem item : menuItems) {

            if (id.equals (item.getId ())) return item;

        }

        for (Menu child : subMenus) {

            MenuItem found = child.findItem (id);

            if (found != null) return found;

        }

        return null;

    }

    public String getTitle () {
        return title;
    }

    public void setTitle (String title) {
        this.title = title;
    }
}
