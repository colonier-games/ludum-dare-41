package games.colonier.ld41.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.MathUtils;
import games.colonier.ld41.config.DifficultyConfig;
import games.colonier.ld41.state.spec.PlayState;
import games.colonier.ld41.entity.Player;

public class KeyboardAndMousePlayerInputHandler extends PlayerInputHandler {

    public int moveUpKey = Input.Keys.W;
    public int moveDownKey = Input.Keys.S;
    public int moveRightKey = Input.Keys.D;
    public int moveLeftKey = Input.Keys.A;

    public int rotateRightKey = Input.Keys.E;
    public int rotateLeftKey = Input.Keys.Q;

    public KeyboardAndMousePlayerInputHandler (
            PlayState playState, Player assignedPlayer
    ) {
        super (playState, assignedPlayer);
    }

    @Override
    public void handleInput () {

        if (Gdx.input.isKeyPressed (moveUpKey)) {
            assignedPlayer.movement.moveUp = true;
        } else {
            assignedPlayer.movement.moveUp = false;
        }

        if (Gdx.input.isKeyPressed (moveDownKey)) {
            assignedPlayer.movement.moveDown = true;
        } else {
            assignedPlayer.movement.moveDown = false;
        }

        if (Gdx.input.isKeyPressed (moveRightKey)) {
            assignedPlayer.movement.moveRight = true;
        } else {
            assignedPlayer.movement.moveRight = false;
        }

        if (Gdx.input.isKeyPressed (moveLeftKey)) {
            assignedPlayer.movement.moveLeft = true;
        } else {
            assignedPlayer.movement.moveLeft = false;
        }

        if (!DifficultyConfig.getInstance ().getDifficulty ().mirrorIsAlwaysOn) {
            assignedPlayer.playerReflector.active = Gdx.input.isButtonPressed (Input.Buttons.LEFT);
        }

        assignedPlayer.rotation = MathUtils.atan2 (
                playState.getMouseWorldCoords ().y - assignedPlayer.position.y,
                playState.getMouseWorldCoords ().x - assignedPlayer.position.x
        ) * MathUtils.radiansToDegrees;

    }

}
