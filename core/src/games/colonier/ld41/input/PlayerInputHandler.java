package games.colonier.ld41.input;

import games.colonier.ld41.state.spec.PlayState;
import games.colonier.ld41.entity.Player;

public abstract class PlayerInputHandler {

    final PlayState playState;
    final Player assignedPlayer;

    public PlayerInputHandler (PlayState playState, Player assignedPlayer) {
        this.playState = playState;
        this.assignedPlayer = assignedPlayer;
    }

    public abstract void handleInput ();

}
