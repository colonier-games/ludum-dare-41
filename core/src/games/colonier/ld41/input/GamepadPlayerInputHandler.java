package games.colonier.ld41.input;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.config.DifficultyConfig;
import games.colonier.ld41.state.spec.PlayState;
import games.colonier.ld41.entity.Player;

public class GamepadPlayerInputHandler extends PlayerInputHandler {

    public int movementAxisX = 1;
    public int movementAxisY = 0;
    public int rotationAxisX = 2;
    public int rotationAxisY = 3;

    public float deadzone = 0.1f;

    final Controller gamepad;

    Vector2 tmp = new Vector2 ();

    public GamepadPlayerInputHandler (
            PlayState playState, Player assignedPlayer,
            Controller gamepad
    ) {
        super (playState, assignedPlayer);
        this.gamepad = gamepad;

    }

    @Override
    public void handleInput () {

        float movementX = gamepad.getAxis (movementAxisX);
        float movementY = gamepad.getAxis (movementAxisY);

        assignedPlayer.movement.moveUp = movementY + deadzone < 0f;
        assignedPlayer.movement.moveDown = movementY - deadzone > 0f;
        assignedPlayer.movement.moveLeft = movementX + deadzone < 0f;
        assignedPlayer.movement.moveRight = movementX - deadzone > 0f;

        float rotationX = gamepad.getAxis (rotationAxisX);
        float rotationY = gamepad.getAxis (rotationAxisY);

        tmp.set (rotationX, rotationY).nor ();

        assignedPlayer.rotation = tmp.angle () + 270f;

        if (!DifficultyConfig.getInstance ().getDifficulty ().mirrorIsAlwaysOn) {
            assignedPlayer.playerReflector.active = gamepad.getButton (5);
        }

    }
}
