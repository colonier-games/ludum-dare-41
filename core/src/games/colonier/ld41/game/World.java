package games.colonier.ld41.game;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.entity.GameEntity;
import games.colonier.ld41.entity.Player;
import games.colonier.ld41.laser.RectangularAbsorber;
import games.colonier.ld41.map.GameMap;
import games.colonier.ld41.map.GameMapGraph;
import games.colonier.ld41.util.FileUtils;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class World implements Serializable {

    public static final String TMX_FILENAME = "levels/laboratory.tmx";
    public GameMap gameMap;
    public List <Player> players = new ArrayList <> ();
    public List <GameEntity> entities = new ArrayList <> ();
    public World () {

        File mapFile = new File (TMX_FILENAME);
        String mapMd5Hash = FileUtils.calcMd5Base64OfFile (mapFile);

        TmxMapLoader tmxMapLoader = new TmxMapLoader ();

        gameMap = new GameMap (
                tmxMapLoader.load (TMX_FILENAME)
        );

        gameMap.tmxMd5Hash = mapMd5Hash;

        final MapLayer[] requiredLayers = { null, null };
        final TiledMapTileLayer[] requiredTileLayers = { null };

        gameMap.tiledMap
                .getLayers ()
                .getByType (TiledMapTileLayer.class)
                .forEach (
                        layer -> {

                            if ("wall-layer".equalsIgnoreCase (layer.getName ())) {

                                requiredTileLayers[0] = layer;

                            }

                        }
                );

        if (requiredTileLayers[0] == null) {

            throw new IllegalArgumentException ("Invalid map, no 'wall-layer' tile layer present!");

        }

        TiledMapTileLayer wallLayer = requiredTileLayers[0];

        gameMap.tileCollidability = new boolean[wallLayer.getHeight ()][wallLayer.getWidth ()];

        for (int y = 0; y < wallLayer.getHeight (); y++) {

            for (int x = 0; x < wallLayer.getWidth (); x++) {

                TiledMapTileLayer.Cell cell = wallLayer.getCell (x, y);

                if (cell != null) {

                    gameMap.mapAbsorbers
                            .add (
                                    new RectangularAbsorber (
                                            new Vector2 (
                                                    x * Numbers.TILE_SCALE + Numbers.TILE_SCALE / 2f,
                                                    y * Numbers.TILE_SCALE + Numbers.TILE_SCALE / 2f
                                            ),
                                            new Vector2 (
                                                    Numbers.TILE_SCALE / 2f,
                                                    Numbers.TILE_SCALE / 2f
                                            )
                                    )
                            );

                    gameMap.tileCollidability[y][x] = false;

                } else {

                    gameMap.tileCollidability[y][x] = true;

                }

            }

        }

        gameMap.tiledMap
                .getLayers ()
                .getByType (MapLayer.class)
                .forEach (
                        objectLayer -> {

                            if ("spawn-points".equalsIgnoreCase (objectLayer.getName ())) {

                                requiredLayers[0] = objectLayer;

                            } else if ("map-objects".equalsIgnoreCase (objectLayer.getName ())) {

                                requiredLayers[1] = objectLayer;

                            }

                        }
                );

        for (int i = 0; i < requiredLayers.length; i++) {
            if (requiredLayers[i] == null) {
                throw new IllegalArgumentException ("Invalid map, no 'spawn-points' and 'map-objects' layer!");
            }
        }

        MapLayer spawnPointsLayer = requiredLayers[0];
        MapLayer mapObjectsLayer = requiredLayers[1];

        spawnPointsLayer.getObjects ()
                .forEach (
                        spawnPoint -> {

                            float spawnPointX = spawnPoint
                                    .getProperties ()
                                    .get ("x", Float.class);

                            float spawnPointY = spawnPoint
                                    .getProperties ()
                                    .get ("y", Float.class);

                            String spawnPointType = spawnPoint
                                    .getProperties ()
                                    .get ("type", "enemy", String.class);

                            if ("player".equalsIgnoreCase (spawnPointType)) {

                                gameMap.playerSpawnPoint.set (
                                        new Vector2 (
                                                spawnPointX * ( Numbers.TILE_SCALE / 16f ),
                                                spawnPointY * ( Numbers.TILE_SCALE / 16f )
                                        )
                                );

                            } else {

                                gameMap.enemySpawnPoints.add (
                                        new Vector2 (
                                                spawnPointX * ( Numbers.TILE_SCALE / 16f ),
                                                spawnPointY * ( Numbers.TILE_SCALE / 16f )
                                        )
                                );

                            }

                        }
                );

        mapObjectsLayer.getObjects ()
                .forEach (
                        mapObject -> {

                            String type = mapObject.getProperties ()
                                    .get ("type", "UNKNOWN", String.class);

                            if ("laser".equalsIgnoreCase (type)) {

                                TiledMapTileMapObject tiledMapTileMapObject
                                        = ( (TiledMapTileMapObject) mapObject );

                                float laserX = mapObject.getProperties ()
                                        .get ("x", Float.class);

                                float laserY = mapObject.getProperties ()
                                        .get ("y", Float.class);

                                String orientation = tiledMapTileMapObject
                                        .getProperties ()
                                        .get ("orientation", "INVALID", String.class);

                                if ("INVALID".equals (orientation)) {

                                    orientation = tiledMapTileMapObject
                                            .getTile ()
                                            .getProperties ()
                                            .get ("orientation", "INVALID", String.class);

                                }

                                if ("INVALID".equals (orientation)) return;

                                gameMap.laser (
                                        (int) ( laserX / 16.0f ),
                                        (int) ( laserY / 16.0f ),
                                        "NORTH".equals (orientation)
                                                ? Numbers.DIR_NORTH
                                                : "SOUTH".equals (orientation)
                                                ? Numbers.DIR_SOUTH
                                                : "EAST".equals (orientation)
                                                ? Numbers.DIR_EAST
                                                : Numbers.DIR_WEST,
                                        10000f
                                );

                            } else if ("reflector".equalsIgnoreCase (type)) {

                                PolylineMapObject polylineObject = (PolylineMapObject) mapObject;
                                Polyline polyline = polylineObject.getPolyline ();
                                final float[] verts = polyline.getVertices ();

                                float reflectorX = mapObject.getProperties ()
                                        .get ("x", Float.class);

                                reflectorX = Math.min (
                                        reflectorX,
                                        reflectorX + verts [2]
                                );

                                float reflectorY = mapObject.getProperties ()
                                        .get ("y", Float.class);

                                reflectorY = Math.max (
                                        reflectorY,
                                        reflectorY + verts [3]
                                );

                                int reflectorDir =
                                        ( verts[2] > 0 ? Numbers.DIR_EAST : verts[2] < 0 ? Numbers.DIR_WEST : 0 )
                                                |
                                                ( verts[3] > 0 ? Numbers.DIR_SOUTH : verts[3] < 0 ? Numbers.DIR_NORTH : 0 );

                                float reflectorWidth =
                                        Numbers.TILE_SCALE / 2f;

                                gameMap.reflector (
                                        (int) ( reflectorX / 16f ),
                                        (int) ( reflectorY / 16f ) - 1,
                                        reflectorDir,
                                        reflectorWidth
                                );

                            }

                        }
                );

        gameMap.gameMapGraph = GameMapGraph.ofTileCollidabilityArray (
                gameMap.tileCollidability
        );

    }

}
