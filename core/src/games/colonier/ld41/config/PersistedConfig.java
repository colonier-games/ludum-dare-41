package games.colonier.ld41.config;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Serializable;

public class PersistedConfig implements Serializable {

    private static final PersistedConfig INSTANCE = new PersistedConfig ();

    private PersistedConfig () {

    }

    private static File getConfigFile () {
        return new File ("config.json");
    }

    public static void load () {

        try {

            File configFile = getConfigFile ();

            if (configFile.exists ()) {

                JsonReader jsonReader = new JsonReader ();

                JsonValue json = jsonReader.parse (
                        new FileInputStream (configFile)
                );

                GraphicsConfig.getInstance ()
                        .parse (json.get ("graphics"));

            } else {

                save ();

            }

        } catch (Exception exc) {

            throw new RuntimeException ("Cannot initialize persisted configuration! Reason: ", exc);

        }

    }

    public static void save () {

        try {

            JsonValue jsonValue = new JsonValue (JsonValue.ValueType.object);

            jsonValue.addChild (
                    "graphics",
                    GraphicsConfig.getInstance ()
                            .save ()
            );

            FileWriter fileWriter = new FileWriter (getConfigFile ());
            fileWriter.write (jsonValue.toJson (JsonWriter.OutputType.json));
            fileWriter.close ();

        } catch (Exception exc) {

            System.err.println (
                    "Couldn't persist configuration! Reason: "
            );

            exc.printStackTrace ();

        }

    }

}
