package games.colonier.ld41.config;

public final class ScoresConfig {

    private static ScoresConfig INSTANCE = new ScoresConfig ();

    public int currentScore = 0;
    public int highScore = 0;

    private ScoresConfig () {}

    public static ScoresConfig getInstance () {
        return INSTANCE;
    }

}
