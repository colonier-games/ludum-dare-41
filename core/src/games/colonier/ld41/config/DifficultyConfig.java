package games.colonier.ld41.config;

public final class DifficultyConfig {

    public enum Difficulty {

        EASY (
                8f,
                8f,
                2f,
                1.2f,
                true,
                1f,
                20f,
                2f,
                10,
                2f
        ),
        NORMAL (
                8f,
                4f,
                4f,
                1.35f,
                false,
                1f,
                10f,
                4f,
                12,
                2f
        ),
        HARD (
                6f,
                3f,
                4f,
                1.5f,
                false,
                2f,
                10f,
                5f,
                15,
                1f
        ),
        BRUTAL (
                4f,
                1f,
                4f,
                2f,
                false,
                4f,
                10f,
                15f,
                20,
                1f
        );

        public final float playerSpeedWithoutReflector;
        public final float playerSpeedWithReflector;
        public final float enemySpeed;
        public final float spawnIncrementFactor;
        public final boolean mirrorIsAlwaysOn;
        public final float beamDamage;
        public final float playerHealth;
        public final float enemyHealth;
        public final int initialNumberOfEnemies;
        public final float timeBetweenSpawns;

        Difficulty (
                float playerSpeedWithoutReflector, float playerSpeedWithReflector, float enemySpeed,
                float spawnIncrementFactor,
                boolean mirrorIsAlwaysOn,
                float beamDamage,
                float playerHealth,
                float enemyHealth,
                int initialNumberOfEnemies,
                float timeBetweenSpawns
        ) {
            this.playerSpeedWithoutReflector = playerSpeedWithoutReflector;
            this.playerSpeedWithReflector = playerSpeedWithReflector;
            this.enemySpeed = enemySpeed;
            this.spawnIncrementFactor = spawnIncrementFactor;
            this.mirrorIsAlwaysOn = mirrorIsAlwaysOn;
            this.beamDamage = beamDamage;
            this.playerHealth = playerHealth;
            this.enemyHealth = enemyHealth;
            this.initialNumberOfEnemies = initialNumberOfEnemies;
            this.timeBetweenSpawns = timeBetweenSpawns;
        }
    }

    private Difficulty difficulty = Difficulty.NORMAL;

    private static DifficultyConfig INSTANCE = new DifficultyConfig ();

    private DifficultyConfig () {

    }

    public static DifficultyConfig getInstance () {
        return INSTANCE;
    }

    public Difficulty getDifficulty () {
        return difficulty;
    }

    public void setDifficulty (Difficulty dif) {
        this.difficulty = dif;
    }

}
