package games.colonier.ld41.config;

import java.util.concurrent.atomic.AtomicInteger;

public final class EntityConfig {

    private AtomicInteger idSequence = new AtomicInteger ();

    private static EntityConfig INSTANCE = new EntityConfig ();

    private EntityConfig () {}

    public int newId () {
        return idSequence.incrementAndGet ();
    }

    public static EntityConfig getInstance () {
        return INSTANCE;
    }

}
