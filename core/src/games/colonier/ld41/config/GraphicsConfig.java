package games.colonier.ld41.config;

import com.badlogic.gdx.utils.JsonValue;
import games.colonier.ld41.constants.Numbers;

import java.io.Serializable;

public class GraphicsConfig implements Serializable {

    private static final GraphicsConfig INSTANCE = new GraphicsConfig ();

    private int resolutionWidth = 1280;
    private int resolutionHeight = 720;
    private boolean isFullScreen = false;
    private int tileScale = 2;
    private boolean vsyncEnabled = false;
    private int aaLevel = 0;

    private GraphicsConfig () {

    }

    void parse (JsonValue json) {

        resolutionWidth = json.getInt ("resolutionWidth");
        resolutionHeight = json.getInt ("resolutionHeight");
        tileScale = json.getInt ("tileScale");
        vsyncEnabled = json.getBoolean ("vsyncEnabled");
        aaLevel = json.getInt ("aaLevel");
        isFullScreen = json.getBoolean ("isFullScreen");

        Numbers.TILE_SCALE = tileScale * 16f;

    }

    JsonValue save () {

        JsonValue value = new JsonValue (JsonValue.ValueType.object);

        value.addChild (
                "resolutionWidth",
                new JsonValue (resolutionWidth)
        );
        value.addChild (
                "resolutionHeight",
                new JsonValue (resolutionHeight)
        );
        value.addChild (
                "tileScale",
                new JsonValue (tileScale)
        );
        value.addChild (
                "vsyncEnabled",
                new JsonValue (vsyncEnabled)
        );
        value.addChild (
                "aaLevel",
                new JsonValue (aaLevel)
        );
        value.addChild (
                "isFullScreen",
                new JsonValue (isFullScreen)
        );

        return value;

    }

    public static GraphicsConfig getInstance () {
        return INSTANCE;
    }

    public int getResolutionWidth () {
        return resolutionWidth;
    }

    public void setResolutionWidth (int resolutionWidth) {
        this.resolutionWidth = resolutionWidth;
        PersistedConfig.save ();
    }

    public int getResolutionHeight () {
        return resolutionHeight;
    }

    public void setResolutionHeight (int resolutionHeight) {
        this.resolutionHeight = resolutionHeight;
        PersistedConfig.save ();
    }

    public int getTileScale () {
        return tileScale;
    }

    public void setTileScale (int tileScale) {
        this.tileScale = tileScale;
        PersistedConfig.save ();
    }

    public boolean isVsyncEnabled () {
        return vsyncEnabled;
    }

    public void setVsyncEnabled (boolean vsyncEnabled) {
        this.vsyncEnabled = vsyncEnabled;
        PersistedConfig.save ();
    }

    public int getAaLevel () {
        return aaLevel;
    }

    public void setAaLevel (int aaLevel) {
        this.aaLevel = aaLevel;
        PersistedConfig.save ();
    }

    public boolean isFullScreen () {
        return isFullScreen;
    }

    public void setFullScreen (boolean fullScreen) {
        isFullScreen = fullScreen;
        PersistedConfig.save ();
    }
}
