package games.colonier.ld41.config;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class InputConfig implements Serializable {

    private static InputConfig INSTANCE = new InputConfig ();

    private boolean joystickAvailable;
    private boolean useJoystick;
    private String selectedJoystickName;
    private List <String> availableJoysticks = new ArrayList <> ();

    private InputConfig () {

    }

    public static InputConfig getInstance () {
        return INSTANCE;
    }

    public void init () {

        joystickAvailable = Controllers.getControllers ().size > 0;
        useJoystick = false;
        availableJoysticks.clear ();
        availableJoysticks.addAll (
                Arrays.stream (
                        Controllers.getControllers ()
                                .toArray ()
                )
                        .map (Controller::getName)
                        .collect (Collectors.toList ())
        );
        selectedJoystickName = availableJoysticks.isEmpty () ? null : availableJoysticks.get (0);

    }

    public boolean isJoystickAvailable () {
        return joystickAvailable;
    }

    public void setJoystickAvailable (boolean joystickAvailable) {
        this.joystickAvailable = joystickAvailable;
    }

    public boolean isUseJoystick () {
        return useJoystick;
    }

    public void setUseJoystick (boolean useJoystick) {
        this.useJoystick = useJoystick;
    }

    public String getSelectedJoystickName () {
        return selectedJoystickName;
    }

    public void setSelectedJoystickName (String selectedJoystickName) {
        this.selectedJoystickName = selectedJoystickName;
    }

    public List <String> getAvailableJoysticks () {
        return availableJoysticks;
    }

    public void setAvailableJoysticks (List <String> availableJoysticks) {
        this.availableJoysticks = availableJoysticks;
    }
}
