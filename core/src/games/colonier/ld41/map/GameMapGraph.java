package games.colonier.ld41.map;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameMapGraph implements IndexedGraph <GameMapGraphNode> {

    public List <GameMapGraphNode> nodes = new ArrayList <> ();
    Map <TileCoords, Integer> tileCoordsIndices = new HashMap <> ();

    public static GameMapGraph ofTileCollidabilityArray (
            boolean[][] tileColladibility
    ) {

        GameMapGraph graph = new GameMapGraph ();

        for (int y = 0; y < tileColladibility.length; y++) {

            for (int x = 0; x < tileColladibility[y].length; x++) {

                if (tileColladibility [y][x]) {

                    graph.tileCoordsIndices.put (
                            new TileCoords (x, y),
                            graph.nodes.size ()
                    );

                    graph.nodes.add (
                            new GameMapGraphNode (
                                    new TileCoords (x, y)
                            )
                    );

                }

            }

        }


        for (int y = 0; y < tileColladibility.length; y++) {

            for (int x = 0; x < tileColladibility[y].length; x++) {

                TileCoords tc = new TileCoords (x, y);

                if (graph.tileCoordsIndices.containsKey (tc)) {

                    GameMapGraphNode node = graph.nodes.get (
                            graph.tileCoordsIndices.get (tc)
                    );

                    if (graph.tileCoordsIndices.containsKey (tc.left ())) {
                        node.neighbours.add (
                                graph.nodes.get (
                                        graph.tileCoordsIndices.get (tc.left ())
                                )
                        );
                    }


                    if (graph.tileCoordsIndices.containsKey (tc.right ())) {
                        node.neighbours.add (
                                graph.nodes.get (
                                        graph.tileCoordsIndices.get (tc.right ())
                                )
                        );
                    }


                    if (graph.tileCoordsIndices.containsKey (tc.up ())) {
                        node.neighbours.add (
                                graph.nodes.get (
                                        graph.tileCoordsIndices.get (tc.up ())
                                )
                        );
                    }


                    if (graph.tileCoordsIndices.containsKey (tc.down ())) {
                        node.neighbours.add (
                                graph.nodes.get (
                                        graph.tileCoordsIndices.get (tc.down ())
                                )
                        );
                    }

                }

            }

        }

        for (int i = 0; i < graph.nodes.size (); i++) {
            graph.nodes.get (i).index = i;
        }

        return graph;

    }

    public GameMapGraphNode getAtCoords (int x, int y) {
        return getAtCoords (new TileCoords (x, y));
    }

    public GameMapGraphNode getAtCoords (TileCoords tc) {
        if (tileCoordsIndices.containsKey (tc)) {
            return nodes.get (
                    tileCoordsIndices.get (tc)
            );
        }
        return null;
    }

    @Override
    public int getIndex (GameMapGraphNode node) {
        return node.index;
    }

    @Override
    public int getNodeCount () {
        return nodes.size ();
    }

    @Override
    public Array <Connection <GameMapGraphNode>> getConnections (
            GameMapGraphNode fromNode
    ) {
        return fromNode.getConnections ();
    }

}
