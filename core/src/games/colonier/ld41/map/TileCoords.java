package games.colonier.ld41.map;

import com.badlogic.gdx.utils.Pool;

import java.io.Serializable;
import java.util.Objects;

public final class TileCoords implements Serializable, Pool.Poolable {

    public int x;
    public int y;

    public TileCoords (int x, int y) {
        this.x = x;
        this.y = y;
    }

    public TileCoords left () {
        return new TileCoords (x - 1, y);
    }

    public TileCoords up () {
        return new TileCoords (x, y + 1);
    }

    public TileCoords down () {
        return new TileCoords (x, y - 1);
    }

    public TileCoords right () {
        return new TileCoords (x + 1, y);
    }

    public TileCoords set (int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (!( o instanceof TileCoords )) return false;
        TileCoords that = (TileCoords) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode () {

        return Objects.hash (x, y);
    }

    @Override
    public String toString () {
        return "TileCoords{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public void reset () {
        x = 0;
        y = 0;
    }
}
