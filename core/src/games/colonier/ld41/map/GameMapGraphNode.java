package games.colonier.ld41.map;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.DefaultConnection;
import com.badlogic.gdx.utils.Array;

import java.util.Objects;

public class GameMapGraphNode {

    public int index;

    public Array <GameMapGraphNode> neighbours = new Array <> ();
    public Array <Connection <GameMapGraphNode>> connections = null;

    public final TileCoords coords;

    public GameMapGraphNode (TileCoords coords) {
        this.coords = coords;
    }

    private void calculateConnections () {

        if (connections == null) {

            connections = new Array <> ();

            for (GameMapGraphNode neighbour : neighbours) {

                connections.add (
                        new DefaultConnection <> (
                                this,
                                neighbour
                        )
                );

            }

        }

    }

    public Array <Connection <GameMapGraphNode>> getConnections () {

        calculateConnections ();

        return connections;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (!( o instanceof GameMapGraphNode )) return false;
        GameMapGraphNode that = (GameMapGraphNode) o;
        return Objects.equals (coords, that.coords);
    }

    @Override
    public int hashCode () {

        return Objects.hash (coords);
    }
}
