package games.colonier.ld41.map;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import games.colonier.ld41.constants.Numbers;

import java.io.Serializable;

public class GameTile implements Serializable, Pool.Poolable {

    private int x;
    private int y;

    private int typeId;

    private boolean positionCalculated = false;
    private Vector2 position = new Vector2 ();

    public GameTile (int typeId, int x, int y) {
        this.x = x;
        this.y = y;
        this.typeId = typeId;
    }

    public int getX () {
        return x;
    }

    public void setX (int x) {
        this.x = x;
    }

    public int getY () {
        return y;
    }

    public void setY (int y) {
        this.y = y;
    }

    public int getTypeId () {
        return typeId;
    }

    public void setTypeId (int typeId) {
        this.typeId = typeId;
    }

    public Vector2 getPosition () {

        if (!positionCalculated) {

            position.set (
                    x * Numbers.TILE_SCALE,
                    y * Numbers.TILE_SCALE
            );

            positionCalculated = true;

        }

        return position;
    }

    @Override
    public void reset () {

        x = 0;
        y = 0;

        position.set (0.0f, 0.0f);
        positionCalculated = false;

    }
}
