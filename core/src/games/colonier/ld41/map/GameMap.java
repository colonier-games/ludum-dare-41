package games.colonier.ld41.map;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld41.constants.Numbers;
import games.colonier.ld41.laser.Absorber;
import games.colonier.ld41.laser.Laser;
import games.colonier.ld41.laser.RectangularAbsorber;
import games.colonier.ld41.laser.Reflector;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GameMap implements Serializable {

    public List <Reflector> mapReflectors = new ArrayList <> ();
    public List <Laser> mapLasers = new ArrayList <> ();
    public List <Absorber> mapAbsorbers = new ArrayList <> ();

    public List <GameTile> tiles = new ArrayList <> ();

    public Vector2 playerSpawnPoint = new Vector2 ();
    public List <Vector2> enemySpawnPoints = new ArrayList <> ();

    public final TiledMap tiledMap;
    public String tmxMd5Hash;

    public boolean [][] tileCollidability;

    public GameMapGraph gameMapGraph;

    public static int TILE_TYPE_FLOOR = 0;

    public GameMap (TiledMap tiledMap) {
        this.tiledMap = tiledMap;
    }

    public GameMap tile (GameTile tile) {

        if (tiles.stream ()
                .noneMatch (t -> t.getX () == tile.getX () && t.getY () == tile.getY ())) {
            tiles.add (tile);
        }
        return this;

    }

    public GameMap fillTiles (int sx, int sy, int ex, int ey, int tileType) {

        for (int y = sy; y < ey; y++) {

            for (int x = sx; x < ex; x++) {

                int finalX = x;
                int finalY = y;
                if (tiles.stream ()
                        .noneMatch (t -> t.getX () == finalX && t.getY () == finalY)) {

                    tiles.add (
                            new GameTile (tileType, x, y)
                    );

                }

            }

        }

        return this;

    }

    public GameMap laser (int x, int y, int dir, float length) {

        mapLasers.add (
                new Laser (
                        new Vector2 (
                                Numbers.TILE_SCALE * x + Numbers.TILE_SCALE / 2f, Numbers.TILE_SCALE * y + Numbers.TILE_SCALE / 2f),
                        new Vector2 (
                                ( dir & Numbers.DIR_EAST ) > 0 ? 1f : ( dir & Numbers.DIR_WEST ) > 0 ? -1f : 0f,
                                ( dir & Numbers.DIR_NORTH ) > 0 ? 1f : ( dir & Numbers.DIR_SOUTH ) > 0 ? -1f : 0f
                        ).nor (),
                        length
                )
        );

        return this;

    }

    public GameMap reflector (int x, int y, int dir, float width) {

        mapReflectors.add (
                new Reflector (
                        new Vector2 (
                                Numbers.TILE_SCALE * x + Numbers.TILE_SCALE / 2f, Numbers.TILE_SCALE * y + Numbers.TILE_SCALE / 2f),
                        new Vector2 (
                                ( dir & Numbers.DIR_EAST ) > 0 ? 1f : ( dir & Numbers.DIR_WEST ) > 0 ? -1f : 0f,
                                ( dir & Numbers.DIR_NORTH ) > 0 ? 1f : ( dir & Numbers.DIR_SOUTH ) > 0 ? -1f : 0f
                        ).nor (),
                        width
                )
        );

        return this;

    }

    public GameMap borderAbsorber () {

        int minTileX = Integer.MAX_VALUE;
        int maxTileX = Integer.MIN_VALUE;

        int minTileY = Integer.MAX_VALUE;
        int maxTileY = Integer.MIN_VALUE;

        for (GameTile tile : tiles) {

            if (tile.getX () < minTileX) minTileX = tile.getX ();
            if (tile.getX () > maxTileX) maxTileX = tile.getX ();

            if (tile.getY () < minTileY) minTileY = tile.getY ();
            if (tile.getY () > maxTileY) maxTileY = tile.getY ();

        }

        Absorber topAbsorber = new RectangularAbsorber (
                new Vector2 (
                        ( (float) minTileX + (float) ( maxTileX - minTileX ) / 2f ) * Numbers.TILE_SCALE,
                        ( maxTileY + 2 ) * Numbers.TILE_SCALE
                ),
                new Vector2 (
                        ( maxTileX - minTileX ) * Numbers.TILE_SCALE,
                        Numbers.TILE_SCALE
                )
        );
        Absorber rightAbsorber = new RectangularAbsorber (
                new Vector2 (
                        ( maxTileX + 2 ) * Numbers.TILE_SCALE,
                        ( (float) minTileY + (float) ( maxTileY - minTileY ) / 2f ) * Numbers.TILE_SCALE
                ),
                new Vector2 (
                        Numbers.TILE_SCALE,
                        ( maxTileY - minTileY ) * Numbers.TILE_SCALE
                )
        );
        Absorber bottomAbsorber = new RectangularAbsorber (
                new Vector2 (
                        ( (float) minTileX + (float) ( maxTileX - minTileX ) / 2f ) * Numbers.TILE_SCALE,
                        ( minTileY - 1 ) * Numbers.TILE_SCALE
                ),
                new Vector2 (
                        ( maxTileX - minTileX ) * Numbers.TILE_SCALE,
                        Numbers.TILE_SCALE
                )
        );
        Absorber leftAbsorber = new RectangularAbsorber (
                new Vector2 (
                        ( minTileX - 1 ) * Numbers.TILE_SCALE,
                        ( (float) minTileY + (float) ( maxTileY - minTileY ) / 2f ) * Numbers.TILE_SCALE
                ),
                new Vector2 (
                        Numbers.TILE_SCALE,
                        ( maxTileY - minTileY ) * Numbers.TILE_SCALE
                )
        );

        mapAbsorbers.add (topAbsorber);
        mapAbsorbers.add (rightAbsorber);
        mapAbsorbers.add (bottomAbsorber);
        mapAbsorbers.add (leftAbsorber);

        return this;

    }

}
