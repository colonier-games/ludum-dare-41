package games.colonier.ld41.annotation;

import games.colonier.ld41.constants.Strings;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention (RetentionPolicy.RUNTIME)
public @interface Controller {

    int order ();
    String [] gameStates () default {
            Strings.GAME_STATE_PLAY
    };

}
