package games.colonier.ld41.util;

import com.badlogic.gdx.graphics.Color;

public final class ColorUtils {

    private ColorUtils () { throw new UnsupportedOperationException (); }

    public static Color parseHex (String hex) {

        if (hex == null
                || !hex.matches ("[0-9a-fA-F]{6,8}")
                || !(hex.length () == 6 || hex.length () == 8)) {

            throw new IllegalArgumentException ("Invalid RGB hex");

        }

        String redPart = hex.substring (0, 2);
        String greenPart = hex.substring (2, 4);
        String bluePart = hex.substring (4, 6);
        String alphaPart = hex.length () == 8 ? hex.substring (6, 8) : "FF";

        return new Color (
                (float) Integer.parseInt (redPart, 16) / 255f,
                (float) Integer.parseInt (greenPart, 16) / 255f,
                (float) Integer.parseInt (bluePart, 16) / 255f,
                (float) Integer.parseInt (alphaPart, 16) / 255f
        );

    }

}
