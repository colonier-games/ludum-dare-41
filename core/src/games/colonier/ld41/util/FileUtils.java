package games.colonier.ld41.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.Base64;

public final class FileUtils {

    private FileUtils () { throw new UnsupportedOperationException (); }

    public static String calcMd5Base64OfFile (String path) {

        return calcMd5Base64OfFile (new File (path));

    }

    public static String calcMd5Base64OfFile (File f) {

        try {

            MessageDigest md = MessageDigest.getInstance ("MD5");

            try (
                    FileInputStream fis = new FileInputStream (f);
                    DigestInputStream dis = new DigestInputStream (fis, md)
                    ) {

                while (dis.read () > 0) {}

            }

            return Base64.getEncoder ().encodeToString (md.digest ());

        } catch (Exception exc) {

            exc.printStackTrace ();

        }

        return null;

    }

    public static String calcMd5Base64 (InputStream is) {

        try {

            MessageDigest md = MessageDigest.getInstance ("MD5");

            try (
                    DigestInputStream dis = new DigestInputStream (is, md)
            ) {

                while (dis.read () > 0) {}

            }

            return Base64.getEncoder ().encodeToString (md.digest ());

        } catch (Exception exc) {

            exc.printStackTrace ();

        }

        return null;

    }

}
