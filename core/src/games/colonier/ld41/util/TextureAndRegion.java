package games.colonier.ld41.util;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public final class TextureAndRegion {

    public Texture texture;
    public TextureRegion textureRegion;

}
