package games.colonier.ld41.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;
import java.util.List;

public final class TextureUtils {

    private TextureUtils () { throw new UnsupportedOperationException (); }

    public static TextureRegion [] loadSingleRowAnimationFrames (
            String textureName,
            int tw, int th,
            int startCol, int endCol
    ) {

        Texture texture = new Texture (Gdx.files.internal (textureName));

        TextureRegion textureRegion = new TextureRegion (texture);

        TextureRegion[][] splitted = textureRegion.split (tw, th);

        List<TextureRegion> result = new ArrayList<> ();

        for (int i = Math.min (startCol, splitted [0].length); i < Math.min (endCol, splitted [0].length); i++) {

            result.add (splitted [0] [i]);

        }

        return result.toArray (new TextureRegion[result.size ()]);

    }

    public static TextureRegion [] loadSingleRowAnimationFrames (
            String textureName,
            int tw, int th,
            int startCol
    ) {

        return loadSingleRowAnimationFrames (
                textureName, tw, th, startCol, 9999
        );

    }

    public static TextureAndRegion loadAndSetTextureAndRegion (String filename) {

        TextureAndRegion result = new TextureAndRegion ();

        result.texture = new Texture (Gdx.files.internal (filename));
        result.textureRegion = new TextureRegion (result.texture);

        return result;

    }
}
