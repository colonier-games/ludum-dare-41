package games.colonier.ld41.gfx;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import games.colonier.ld41.constants.Numbers;

public class BeamParticle implements Pool.Poolable {

    public static final float MAX_ALIVE_TIME = 0.5f;
    public static final float ALPHA = Numbers.TILE_SCALE / 4f;

    public float aliveTimer = 0f;

    public Vector2 initialPosition = new Vector2 ();
    public Vector2 position = new Vector2 ();
    public Vector2 direction = new Vector2 ();

    Vector2 tmp = new Vector2 ();
    Vector2 tmp2 = new Vector2 ();

    public void update (float delta) {

        aliveTimer += delta;

        if (alive ()) {

            // initialPosition + direction * ( ALPHA / 2 ) --> circle center

            tmp.set (
                    initialPosition
            ).add (
                    tmp2.set (direction)
                    .nor ()
                    .scl (ALPHA / 2f)
            );

            // tmp is now center of circle

            position.set (
                    tmp
            ).sub (
                    tmp2.set (direction)
                    .nor ()
                    .rotate (( aliveTimer / MAX_ALIVE_TIME) * 90f + 90f)
                    .scl (ALPHA)
            );

        }

    }

    public boolean alive () {
        return aliveTimer < MAX_ALIVE_TIME;
    }

    @Override
    public void reset () {

        initialPosition.set (0f, 0f);
        position.set (0f, 0f);
        direction.set (0f, 0f);
        aliveTimer = 0f;

    }
}
