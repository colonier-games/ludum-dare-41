package games.colonier.ld41.constants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public final class Musics {

    private Musics () { throw new UnsupportedOperationException (); }

    private static boolean initialized = false;

    public static void initialize () {

        if (initialized) {

            throw new IllegalStateException ("Already initialized");

        }

        CALM_MUSIC = Gdx.audio.newMusic (
                Gdx.files.internal ("sounds/music-2.mp3")
        );

        BATTLE_MUSIC = Gdx.audio.newMusic (
                Gdx.files.internal ("sounds/music-1.mp3")
        );

        initialized = true;

    }

    private static Music CALM_MUSIC;
    private static Music BATTLE_MUSIC;

    public static Music CALM_MUSIC () {
        if (!initialized) {
            throw new IllegalStateException ("not yet initialized");
        }
        return CALM_MUSIC;
    }

    public static Music BATTLE_MUSIC () {
        if (!initialized) {
            throw new IllegalStateException ("not yet initialized");
        }
        return BATTLE_MUSIC;
    }

}
