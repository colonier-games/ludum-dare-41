package games.colonier.ld41.constants;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import games.colonier.ld41.util.TextureAndRegion;
import games.colonier.ld41.util.TextureUtils;

public final class ObjectTextures {

    private ObjectTextures () { throw new UnsupportedOperationException (); }

    private static boolean initialized = false;

    public static void initialize () {

        if (initialized) {

            throw new IllegalStateException ("Already initialized!");

        }

        LASER_TEXTURE_AND_REGION = TextureUtils.loadAndSetTextureAndRegion ("textures/laser.png");
        PLAYER_TEXTURE_AND_REGION = TextureUtils.loadAndSetTextureAndRegion ("textures/player.png");
        ENEMY_TEXTURE_AND_REGION = TextureUtils.loadAndSetTextureAndRegion ("textures/enemy-1.png");

        initialized = true;

    }

    private static TextureAndRegion LASER_TEXTURE_AND_REGION;
    private static TextureAndRegion PLAYER_TEXTURE_AND_REGION;
    private static TextureAndRegion ENEMY_TEXTURE_AND_REGION;

    public static Texture LASER_TEXTURE () {
        if (!initialized) {
            throw new IllegalStateException ("Must be initialized first!");
        }
        return LASER_TEXTURE_AND_REGION.texture;
    }

    public static TextureRegion LASER_REGION () {
        if (!initialized) {
            throw new IllegalStateException ("Must be initialized first!");
        }
        return LASER_TEXTURE_AND_REGION.textureRegion;
    }


    public static Texture PLAYER_TEXTURE () {
        if (!initialized) {
            throw new IllegalStateException ("Must be initialized first!");
        }
        return PLAYER_TEXTURE_AND_REGION.texture;
    }

    public static TextureRegion PLAYER_REGION () {
        if (!initialized) {
            throw new IllegalStateException ("Must be initialized first!");
        }
        return PLAYER_TEXTURE_AND_REGION.textureRegion;
    }

    public static Texture ENEMY_TEXTURE () {
        if (!initialized) {
            throw new IllegalStateException ("Must be initialized first!");
        }
        return ENEMY_TEXTURE_AND_REGION.texture;
    }

    public static TextureRegion ENEMY_REGION () {
        if (!initialized) {
            throw new IllegalStateException ("Must be initialized first!");
        }
        return ENEMY_TEXTURE_AND_REGION.textureRegion;
    }

}
