package games.colonier.ld41.constants;

import com.badlogic.gdx.graphics.Color;
import games.colonier.ld41.util.ColorUtils;

public final class Colors {

    private Colors () { throw new UnsupportedOperationException (); }

    public static final Color LASER_BEAM_COLOR = ColorUtils.parseHex ("ff5722");

}
