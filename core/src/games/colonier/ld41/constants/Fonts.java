package games.colonier.ld41.constants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public final class Fonts {

    private Fonts () { throw new UnsupportedOperationException (); }

    private static boolean initialized = false;

    public static void initialize () {

        if (initialized) {
            throw new IllegalStateException ("already initialized!");
        }

        UBUNTU_MONO_REGULAR_32PX = new BitmapFont (
                Gdx.files.internal ("fonts/ubuntu-mono-regular-32/font.fnt")
        );

        UBUNTU_MONO_REGULAR_16PX = new BitmapFont (
                Gdx.files.internal ("fonts/ubuntu-mono-regular-16/font.fnt")
        );

        UBUNTU_MONO_ITALIC_32PX = new BitmapFont (
                Gdx.files.internal ("fonts/ubuntu-mono-italic-32/font.fnt")
        );

        UBUNTU_MONO_ITALIC_16PX = new BitmapFont (
                Gdx.files.internal ("fonts/ubuntu-mono-italic-16/font.fnt")
        );

        UBUNTU_MONO_BOLD_32PX = new BitmapFont (
                Gdx.files.internal ("fonts/ubuntu-mono-bold-32/font.fnt")
        );

        UBUNTU_MONO_BOLD_16PX = new BitmapFont (
                Gdx.files.internal ("fonts/ubuntu-mono-bold-16/font.fnt")
        );

        UBUNTU_MONO_BOLD_32PX.getData ().markupEnabled = true;
        UBUNTU_MONO_REGULAR_16PX.getData ().markupEnabled = true;
        UBUNTU_MONO_ITALIC_32PX.getData ().markupEnabled = true;
        UBUNTU_MONO_ITALIC_16PX.getData ().markupEnabled = true;
        UBUNTU_MONO_BOLD_32PX.getData ().markupEnabled = true;
        UBUNTU_MONO_BOLD_16PX.getData ().markupEnabled = true;

        initialized = true;

    }

    private static BitmapFont UBUNTU_MONO_REGULAR_32PX;
    private static BitmapFont UBUNTU_MONO_REGULAR_16PX;
    private static BitmapFont UBUNTU_MONO_BOLD_32PX;
    private static BitmapFont UBUNTU_MONO_BOLD_16PX;
    private static BitmapFont UBUNTU_MONO_ITALIC_32PX;
    private static BitmapFont UBUNTU_MONO_ITALIC_16PX;

    public static BitmapFont small () {
        return UBUNTU_MONO_REGULAR_16PX ();
    }

    public static BitmapFont smallBold () {
        return UBUNTU_MONO_BOLD_16PX ();
    }

    public static BitmapFont smallItalic () {
        return UBUNTU_MONO_ITALIC_16PX ();
    }

    public static BitmapFont big () {
        return UBUNTU_MONO_REGULAR_32PX ();
    }

    public static BitmapFont bigBold () {
        return UBUNTU_MONO_BOLD_32PX ();
    }

    public static BitmapFont bigItalic () {
        return UBUNTU_MONO_ITALIC_32PX ();
    }

    public static BitmapFont UBUNTU_MONO_REGULAR_32PX () {
        if (!initialized) {
            throw new IllegalStateException ("not initialized yet!");
        }
        return UBUNTU_MONO_REGULAR_32PX;
    }

    public static BitmapFont UBUNTU_MONO_REGULAR_16PX () {
        if (!initialized) {
            throw new IllegalStateException ("not initialized yet!");
        }
        return UBUNTU_MONO_REGULAR_16PX;
    }

    public static BitmapFont UBUNTU_MONO_BOLD_32PX () {
        if (!initialized) {
            throw new IllegalStateException ("not initialized yet!");
        }
        return UBUNTU_MONO_BOLD_32PX;
    }

    public static BitmapFont UBUNTU_MONO_BOLD_16PX () {
        if (!initialized) {
            throw new IllegalStateException ("not initialized yet!");
        }
        return UBUNTU_MONO_BOLD_16PX;
    }

    public static BitmapFont UBUNTU_MONO_ITALIC_32PX () {
        if (!initialized) {
            throw new IllegalStateException ("not initialized yet!");
        }
        return UBUNTU_MONO_ITALIC_32PX;
    }

    public static BitmapFont UBUNTU_MONO_ITALIC_16PX () {
        if (!initialized) {
            throw new IllegalStateException ("not initialized yet!");
        }
        return UBUNTU_MONO_ITALIC_16PX;
    }

}
