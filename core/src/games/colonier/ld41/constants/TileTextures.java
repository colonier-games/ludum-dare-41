package games.colonier.ld41.constants;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import games.colonier.ld41.util.TextureAndRegion;
import games.colonier.ld41.util.TextureUtils;

public final class TileTextures {

    private TileTextures () { throw new UnsupportedOperationException (); }

    private static boolean initialized = false;

    public static void initialize () {

        if (initialized) {

            throw new IllegalStateException ("Already initialized!");

        }

        initialized = true;

    }

}
