package games.colonier.ld41.constants;

public final class Messages {

    private Messages () { throw new UnsupportedOperationException (); }

    public static final int PLAYER_MOVED = 0;

}
