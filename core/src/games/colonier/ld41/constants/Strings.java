package games.colonier.ld41.constants;

public final class Strings {

    public static final String GAME_STATE_MAIN_MENU = "MainMenu";

    private Strings () { throw new UnsupportedOperationException (); }

    public static final String GAME_STATE_PLAY = "Play";

}
