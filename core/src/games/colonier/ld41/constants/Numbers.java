package games.colonier.ld41.constants;

public final class Numbers {

    public static final float PLAYER_SPEED_TILES_PER_SEC = 4f;
    public static final float ENEMY_SPEED_TILES_PER_SEC = 1.5f;
    public static final float BEAM_DAMAGE_PER_SEC = 0.3f;
    public static final float PLAYER_INITIAL_HEALTH = 100.0f;

    public static int DIR_NORTH = 0b1000_0000;
    public static int DIR_EAST = 0b0100_0000;
    public static int DIR_SOUTH = 0b0010_0000;
    public static int DIR_WEST = 0b0001_0000;
    public static float TILE_SCALE = 32.0f;

    private Numbers () { throw new UnsupportedOperationException (); }



}
