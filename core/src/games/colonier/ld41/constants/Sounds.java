package games.colonier.ld41.constants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public final class Sounds {

    private Sounds () { throw new UnsupportedOperationException (); }

    private static boolean initialized = false;

    public static void initialize () {

        if (initialized) {

            throw new IllegalStateException ("Already initialized");

        }

        SPAWN = Gdx.audio.newSound (
                Gdx.files.internal ("sounds/spawn.wav")
        );

        EXPLOSION = Gdx.audio.newSound (
                Gdx.files.internal ("sounds/explosion.wav")
        );

        TICK = Gdx.audio.newSound (
                Gdx.files.internal ("sounds/tick.wav")
        );

        SELECT = Gdx.audio.newSound (
                Gdx.files.internal ("sounds/select.wav")
        );

        initialized = true;

    }

    private static Sound SPAWN;
    private static Sound EXPLOSION;
    private static Sound TICK;
    private static Sound SELECT;

    public static Sound SPAWN () {
        if (!initialized) {
            throw new IllegalStateException ("not yet initialized");
        }
        return SPAWN;
    }

    public static Sound EXPLOSION () {
        if (!initialized) {
            throw new IllegalStateException ("not yet initialized");
        }
        return EXPLOSION;
    }

    public static Sound TICK () {
        if (!initialized) {
            throw new IllegalStateException ("not yet initialized");
        }
        return TICK;
    }

    public static Sound SELECT () {
        if (!initialized) {
            throw new IllegalStateException ("not yet initialized");
        }
        return SELECT;
    }

}
